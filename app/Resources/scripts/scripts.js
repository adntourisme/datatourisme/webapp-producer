/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

window.jQuery = window.$ = require('jquery');
var angular = require("angular");

/** datatourisme front framework **/
require('datatourisme-webapp-bundle');

/** components **/
require('./components/spark');
require('./components/selectize');
require('./components/Morris/morris.donut');
require('./components/Morris/morris.line');
require('./components/btn-group-tabs');

/** apps **/
require('alignment-editor');
require('./ng-app/ruleset-wizard/ruleset-wizard');

// configure alignment-editor
angular.module('alignment-editor')
    .constant('appConfig', {
        debug: false,
        datatourisme: {
            namespace: 'https://www.datatourisme.gouv.fr/ontology/core#',
            endpoints:{}
        }
    });