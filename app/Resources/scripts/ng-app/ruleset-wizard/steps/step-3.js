/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular.module('ruleset-wizard').config(config);

/**
 * @ngInject
 */
function config($stateProvider) {
    $stateProvider.state('wizard.step3', {
        url: "step-3",
        template: require("./step-3.html"),
        controller: controller
    });
}

/**
 * @ngInject
 */
function controller(config, $state, $window, $scope) {
    if($scope.schema == null) {
        return $state.go('wizard.step0');
    }
    if(!$scope.data.xpathType || !$scope.data.xpathId) {
        return $state.go('wizard.step2');
    }

    $scope.$parent.currentStep = 3;

    // init corresps
    if(!$scope.data.corresps) {
        var corresps = {};
        for(var key in $scope.stats.types) {
            corresps[key] = config.thesaurusValues[key] ? config.thesaurusValues[key] : null;
        }
        $scope.data.corresps = corresps;
    }

    $scope.isDisabled = function(key) {
        return !!config.thesaurusValues[key];
    }

    $scope.submit = function() {
        if($scope.form.$valid) {
            // confirmation
            var remainingCorresps = $scope.remainingCorresps().length;
            if(remainingCorresps > 0) {
                var confirm = $window.confirm("Il reste " + remainingCorresps + " types actuellement non alignés. Êtes-vous certain de vouloir continuer ?");
                if(!confirm) {
                    return;
                }
            }
            $scope.$parent.validate();
        }
    }
}