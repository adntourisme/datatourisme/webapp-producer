/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular.module('ruleset-wizard').config(config);

/**
 * @ngInject
 */
function config($stateProvider) {
    $stateProvider.state('wizard.step2', {
        url: "step-2",
        template: require("./step-2.html"),
        controller: controller
    });
}

/**
 * @ngInject
 */
function controller($state, $scope, $http, $q, config) {
    if($scope.schema == null) {
        return $state.go('wizard.step0');
    }
    if(!$scope.data.xpathContext) {
        return $state.go('wizard.step1');
    }

    $scope.$parent.currentStep = 2;
    $scope.typeExtractionMode = 'expr';

    if(!$scope.data.xpathContext) {
        return $state.go('wizard.step1');
    }

    $scope.$watch("data.resourceType", function(resourceType) {
        if(resourceType) {
            delete $scope.data.xpathType;
        }
    });

    $scope.$watch("data.xpathType", function(resourceType) {
        if(resourceType) {
            delete $scope.data.resourceType;
        }
    });

    $scope.submit = function() {
        if($scope.form.$valid) {
            if($scope.data.corresps) {
                $state.go('wizard.step3');
            } else {
                $scope.$parent.validate();
            }
        }
    };
}