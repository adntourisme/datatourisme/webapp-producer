/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var Spark = require("./Spark/Spark");
var $ = require("jquery");

$.fn.initComponents.add(function(dom) {
    $(document).ajaxComplete(function() {
        $("[data-sparkline]", dom).each(function () {
            var self = $(this);

            var values = self.data("sparklineValues");
            var tooltips = self.data("sparklineTooltips");
            var options = self.data("sparklineOptions");

            var parameters = {
                el: self[0],
                values: values ? values : [],
                tooltips: tooltips ? tooltips : []
            };

            var spark = new Spark(
                parameters,
                options ? options : {}
            );

            spark.run();
        });
    });
});