/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var $ = require("jquery");

$.fn.initComponents.add(function(dom) {
    $("[data-selectize]", dom).each(function () {

        var self = $(this);
        var choices = self.data("selectize");
        choices     = choices ? choices : [];

        // ____ Tricky: Formatting Symfony form data to an array of select options understandable by Selectize.js
        var options = [];
        var length  = choices.length;
        var i;
        for (i = 0; i < length; i++)
        {
            options[i] = {
                valueField: choices[i].value,
                labelField: choices[i].label
            };
        }
        // ________

        self.selectize({
            placeholder: "-- Tout --",
            options: options
        });
    });
});