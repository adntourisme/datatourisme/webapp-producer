/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var $ = require("jquery");
require("morris.js/morris");
window.Raphael = require("raphael");

$.fn.initComponents.add(function(dom) {
    $("[data-morris-line]", dom).each(function () {
        var self = $(this);

        var values = self.data("morrisLine");

        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'line',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: values,
            // The name of the data record attribute that contains x-values.
            xkey: 'date',
            xLabels:'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['POI'],
            resize: true,
            dateFormat: function (ts) {
                var d = new Date(ts);
                return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
            },
            xLabelFormat: function(d) {
                return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
            },
            hideHover: 'auto'
        });
    });
});