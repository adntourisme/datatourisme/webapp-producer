Feature: DAT-11 FAQ - Rechercher
  L'utilisateur est en mesure d'effectuer une recherche fulltext dans la FAQ.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  Scenario: L'utilisateur peut saisir un ou plusieurs mots-clés dans le champ de recherche.
    Given I am logged in as "faq@dt.dev" with "faq"
    And I am on "help/faq"
    When I fill in "faq_search[keywords]" with "dolor"
    And I press "search-btn"
    And the response status code should be 200
    And the ".content" element should contain "dolor"

  Scenario: Une recherche qui n'aboutit pas devrait afficher "Aucun résultat".
    Given I am logged in as "faq@dt.dev" with "faq"
    And I am on "help/faq"
    When I fill in "faq_search[keywords]" with "dsqkjdqsdfhjghdfjkghotuergidjksthejrghduigjn"
    And I press "search-btn"
    Then I should be on "help/faq"
    And the response status code should be 200
    Then I should see an ".blankslate > h3" element