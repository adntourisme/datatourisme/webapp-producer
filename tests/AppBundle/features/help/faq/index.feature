Feature: DAT-10 FAQ - Afficher
  L'utilisateur est en mesure d'afficher la FAQ.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  Scenario: Je peux afficher la FAQ.
    Given I am logged in as "faq@dt.dev" with "faq"
    When I go to "help/faq"
    Then I should be on "help/faq"
    And the response status code should be 200