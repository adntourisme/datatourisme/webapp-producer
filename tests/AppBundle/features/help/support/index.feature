Feature: DAT-60 Liste des discussions
  L'utilisateur est en mesure d'afficher la liste des discussions.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  # GLOBAL
  Scenario: Je peux afficher la liste des discussions.
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "help/support"
    Then I should be on "help/support"
    And the response status code should be 200

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher les structures.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/support"
    Then I should see "Structure" in the "thead" element

  Scenario: En tant qu'administrateur, je peux afficher le nom des administrateurs.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/support"
    Then I should see "John DOE"

  Scenario: Les discussions sont paginées.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/support"
    When I follow "2"
    Then the response status code should be 200
    When I follow "3"
    Then the response status code should be 200
    When I follow "4"
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je ne peux pas afficher les structures.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on "help/support"
    Then I should not see "Structure" in the "thead" element

  Scenario: En tant qu'utilisateur, je ne peux pas afficher le nom des administrateurs.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on "help/support"
    Then I should not see "John DOE"

  Scenario: En tant qu'utilisateur, je ne peux lister que les discussions liées à ma structure.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    Then I should see "Dummy Thread 1"
    But I should not see "Dummy Thread 2"