Feature: DAT-62 Participer à une discussion
  L'utilisateur est en mesure de participer à une discussion.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  Scenario: En tant qu'utilisateur, je peux créer un nouveau post dans un fil de discussion appartenant à ma structure.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    And I fill in the following:
      | support_post[content] | Dummy Post 1 |
    And I press "Envoyer"
    Then the response status code should be 200
    And I should not see "Dummy Post 1" in the "#support_post_content" element
    But I should see "Dummy Post 1"

  Scenario: En tant qu'utilisateur, je peux attacher un fichier à un nouveau post.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    And I fill in the following:
      | support_post[content] | Dummy Post 2 |
    And I attach the file "images/dummy-image.jpg" to "support_post[file]"
    And I press "Envoyer"
    Then the response status code should be 200
    And I should not see "Dummy Post 2" in the "#support_post_content" element
    But I should see "Dummy Post 2"

  Scenario: En tant qu'utilisateur, je peux télécharger le fichier attaché à un post.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    And I fill in the following:
      | support_post[content] | Dummy Post 2 |
    And I attach the file "images/dummy-image.jpg" to "support_post[file]"
    And I press "Envoyer"
    When I follow "dummy-image.jpg"
    Then the response status code should be 200
    And the response headers should contain "content-type":"image/jpeg"

  Scenario: En tant qu'utilisateur, je reçois un email lorsqu'un nouveau message auquel je suis lié est posté.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    And I fill in the following:
      | support_post[content] | Dummy Post 3 |
    And I press "Envoyer"
    When I check the last received email
    Then the subject should contain "Dummy Thread 1"
    And the body should contain "Dummy Thread 1"
    When I follow the first link in the last received email
    Then the response status code should be 200
    And I should see "Dummy Post 3"