Feature: DAT-74 Interface d'alignement

  Scenario: Je charge les données
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Je ne peux pas aligner en tant qu'administrateur
    Then I am logged in as "admin@dt.dev" with "admin"
    And I should not see "Thésaurus"
    Then I go to "/thesaurus/align/classes/PointOfInterest"
    Then the response status code should be 403

  Scenario: Je peux aligner en tant que producteur
    Then I am logged in as "user@dt.dev" with "user"
    And I should see "Thésaurus"
    Then I go to "/thesaurus/align/classes/PointOfInterest"
    And I fill in the following:
      | alignment[field_0] | Fête et Manifestation  |
      | alignment[field_1] | Itinéraire touristique |
      | alignment[field_2] | Lieu                   |
      | alignment[field_3] | Produit                |
    And I press "Sauvegarder"
    Then the response status code should be 200

  Scenario: Je vérifie certain champs
    Then I am logged in as "user@dt.dev" with "user"
    Then I go to "/thesaurus/align/individuals/Audience"
    Then I should see "Audience"
    And I should see "Audience de personnes"

    Then I go to "/thesaurus/align/individuals/Amenity"
    Then I should see "Équipement"
    And I should see "Équipement d'hébergement"

    Then I go to "/thesaurus/align/individuals/Theme"
    Then I should see "Thème"
    And I should see "Thème de centre d'interprétation"

    Then I go to "/thesaurus/align/other/NamedIndividual"
    Then I should see "Entités nommées"

  @javascript
  Scenario: Une alerte s'affiche si les modifications ne sont pas enregistrées
    Then I am logged in as "user@dt.dev" with "user"
    Then I go to "/thesaurus/align/classes/PointOfInterest"
    And I fill in the following:
      | alignment[field_0] | test |
    Then I follow "Itinéraire touristique"
    And I wait for 2 seconds
    And I should see "Vous avez effectué des modifications"
    And I wait for 2 seconds
    Then I click the ".modal-dialog .btn-default" element
    And I wait for 2 seconds
    And I should not see "Vous avez effectué des modifications"
    Then I submit the "form" form
    Then I follow "Itinéraire touristique"
    And I fill in the following:
      | alignment[field_0] | modification |
    Then I follow "Fête et Manifestation"
    And I wait for 2 seconds
    And I should see "Vous avez effectué des modifications"
    And I wait for 2 seconds
    Then I click the ".modal-dialog .btn-primary" element
