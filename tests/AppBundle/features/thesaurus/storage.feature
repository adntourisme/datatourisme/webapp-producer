Feature: DAT-75 Import/export de l'alignement
  Scenario: Je charge les données
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Je prépare les données pour l'export
    Then I am logged in as "user@dt.dev" with "user"
    And I should see "Thésaurus"
    Then I go to "thesaurus/align/geographic/France"
    Then I should see "France"
    And I follow "Bretagne"
    And I follow "Ille-et-Vilaine"
    Then I should see "Acigné"
    And I fill in the following:
    | alignment[field_0] | 35690 |
    And I submit the "form" form
    Then the response status code should be 200
    And I should be on "/thesaurus/align/geographic/:France5335"

  Scenario: J'exporte l'alignement
    Then I am logged in as "user@dt.dev" with "user"
    Then I go to "/thesaurus/export/geographic/:France5335/0"
    And the response should contain "'35690'"

  Scenario: J'importe l'alignement mis à jour
    Then I am logged in as "user@dt.dev" with "user"
    Then I go to "/thesaurus/import/geographic/:France5335"
    When I attach the file "thesaurus/geographic.yml" to "form[file]"
    Then I submit the "form" form
    Then the response status code should be 200

  Scenario: Je vérifie la mise à jour des données
    Then I am logged in as "user@dt.dev" with "user"
    Then I go to "thesaurus/align/geographic/:France"
    Then I should see "France"
    And I follow "Bretagne"
    And I follow "Ille-et-Vilaine"
    Then I should see "Acigné"
    Then I should see "35001"
    Then I go to "thesaurus/align/geographic/:France"
    Then I follow "Exporter toutes les correspondances"
    And the response should not contain "'it is not a correct key': 'but a correct value'"