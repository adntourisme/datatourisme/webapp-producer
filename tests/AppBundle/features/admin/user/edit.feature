Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: En tant qu'administrateur, je peux créer un éditer un utilisateur existant
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    When I fill in the following:
      | user[lastName] | Deaux |
    And I press "Enregistrer"
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I should see "DEAUX Jean"