Feature: DAT-41

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: En tant qu'administrateur, je peux télécharger les rapports de connexion
    When I am logged in as "admin@dt.dev" with "admin"
    And I go to "/admin/user"
    Then the response status code should be 200
    Then I follow "Télécharger un rapport de connexions"
    Then I should see "Sélectionnez une période"
    Then I submit the "form" form
    And the response status code should be 200
