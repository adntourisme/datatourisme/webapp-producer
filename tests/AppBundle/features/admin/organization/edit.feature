Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario:
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/organization"
    Then I follow "Dummy Empire"
