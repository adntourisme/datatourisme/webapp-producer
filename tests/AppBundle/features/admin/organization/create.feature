Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario:
    When I am logged in as "admin@dt.dev" with "admin"
    When I go to "/admin/organization/add"
    When I fill in the following:
      | organization[name]        | Conjecto                  |
      | organization[address]     | 29 Rue de Lorient         |
      | organization[zip]         | 35000                     |
      | organization[city]        | Rennes                    |
      | organization[phoneNumber] | 09 80 52 20 21            |
      | organization[website]     | http://www.conjecto.com/  |
    And I press "Enregistrer"
    Then I go to "/admin/organization"
    Then I should see "Conjecto"
