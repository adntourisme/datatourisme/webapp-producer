Feature: Détail d'un flux : Importer un fichier

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | process.yml                    |
      | flux.yml                       |

  Scenario: J'importe un fichier manuellement
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/82/parameters"
    Then the response status code should be 200
    When I fill in the following:
      | flux_details[mode] | manual |
    And I submit the "form" form
    Then the response status code should be 200
    Then I go to "/flux/82/import/manual"
    Then the response status code should be 200
    When I attach the file "datasource/datasource.xml" to "form[file]"
    Then I submit the "form" form
    Then the response status code should be 200

  Scenario: J'importe un fichier en mode pull
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/82/parameters"
    Then the response status code should be 200
    When I fill in the following:
      | flux_details[mode] | pull |
      | flux_details[fluxCrawler][url] | http://127.0.0.1/datasource.xml |
    And I submit the "form" form
    Then the response status code should be 200
