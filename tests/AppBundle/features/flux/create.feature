Feature: Création d'un flux
  En tant qu'utilisateur, je peux créer un flux

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je ne peux pas créer de flux
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "/flux/create"
    Then the response status code should be 403

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux créer un flux
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/flux/conception"
    Then the response status code should be 200
    And I follow "Nouveau flux"
    When I fill in the following:
      | flux[name]        | test |
      | flux[description] | test |
    And I submit the "form" form
    Then the response status code should be 200
    When I go to "/flux/conception"
    Then I should see "test"