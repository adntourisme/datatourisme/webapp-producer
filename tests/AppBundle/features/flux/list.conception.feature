Feature: DAT-81 Liste des flux en cours de conception
  En tant qu'utilisateur, je peux consulter la liste des flux en cours de conception.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher la liste.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "flux/conception"
    Then I should be on "flux/conception"
    And the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "flux/conception"
    When I follow "Structure"
    Then the response status code should be 200
    When I follow "Flux"
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "flux/conception"
    When I fill in "conception_flux_filter_organization" with "1"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher la liste.
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "flux/conception"
    Then I should be on "flux/conception"
    And the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/conception"
    When I follow "Flux"
    Then the response status code should be 200
    When I follow "Statut"
    Then the response status code should be 200
    When I follow "Jeu de règles"
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on "flux/conception"
    When I fill in "conception_flux_filter_status" with "draft"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "conception_flux_filter_name" with "Lorem"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "conception_flux_filter_ruleset" with "draft"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  Scenario: En tant qu'utilisateur, je ne peux pas trier les résultats par structure.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on "flux/conception"
    Then I should not see "Structure" in the "thead" element