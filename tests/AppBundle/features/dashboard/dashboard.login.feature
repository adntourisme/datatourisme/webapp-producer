Feature: Widget dashboard
  L'administrateur est en mesure d'afficher les dernières connections dans un widget du dashboard.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |

  # ADMINISTRATEUR
  Scenario: Je peux afficher le widget des KPI.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/dashboard/widget/login"
    Then the response status code should be 403
    And I go to "/logout"
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "/dashboard/widget/login"
    Then the response status code should be 200
    Then I should see "Jean DEAU"
    Then I follow "Télécharger un rapport de connexions"
    Then I submit the "form" form
    And the response status code should be 200
