Feature: Widget dashboard
  L'utilisateur est en mesure d'afficher les 5 derniers traitements dans un widget du dashboard.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  Scenario: Je peux afficher le widget des traitements.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "/dashboard/widget/production"
    Then the response status code should be 200
    And I should see "Flux en production"

  Scenario: Je ne peux afficher que les discussions de ma structure.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/dashboard/widget/production"
    Then the response status code should be 200
    And I should see "Flux en production"