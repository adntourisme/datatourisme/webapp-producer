Feature: DAT-5 Conditions d'utilisation
  L'utilisateur est en mesure de valider les CGU de sa structure

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Accept cgu
    Given I am logged in as "cgu@dt.dev" with "cgu"
    Then I should be on "/cgu"
    And the "form_accept" checkbox should not be checked
    Given I submit the "form" form
    Then I should be on "/cgu"
    Given I check "form_accept"
    And I submit the "form" form
    Then I should be on "/"
