Feature: DAT-67 Widget navbar
  En tant qu'utilisateur, je retrouve dans la navbar un widget des dernières notifications.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  @javascript
  Scenario: Je peux afficher mes notifications.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on homepage
    Then I wait for 3 seconds
    When I click the ".messages-menu" element
    And I wait for 3 seconds
    Then I should see "Ivan DOV" in the ".messages-menu" element

  @javascript
  Scenario: En tant qu'utilisateur, je ne peux afficher que les notifications liées à ma structure.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on homepage
    Then I wait for 3 seconds
    When I click the ".messages-menu" element
    And I wait for 3 seconds
    Then I should not see "Ivan DOV" in the ".messages-menu" element