Feature: Lancement de traitement pour les flux en production
  Le traitement d'un flux est envoyé au worker, les données sont reçues et traitées

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

   # UTILISATEUR
  Scenario: En tant qu'utilisateur, J'importe un fichier manuellement
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/82/parameters"
    Then the response status code should be 200
    When I fill in the following:
      | flux_details[mode] | manual |
    And I submit the "form" form
    Then the response status code should be 200
    Then I go to "/flux/82/import/manual"
    Then the response status code should be 200
    When I attach the file "datasource/datasource.xml" to "form[file]"
    Then I submit the "form" form
    Then the response status code should be 200