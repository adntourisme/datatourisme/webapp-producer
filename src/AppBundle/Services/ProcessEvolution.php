<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Services;

use AppBundle\Entity\Flux;
use AppBundle\Entity\FluxStatsEntry;
use AppBundle\Repository\FluxStatsEntryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class Process.
 */
class ProcessEvolution
{
    /**
     * Types.
     */
    const SPARKLINE = 101;
    const MORRIS = 102;

    /** @var EntityManager $em */
    protected $em;

    /** @var AuthorizationChecker $authorizationChecker */
    protected $authorizationChecker;

    /** @var TokenStorageInterface $tokenStorage */
    protected $tokenStorage;

    /** @var FluxStatsEntryRepository $fluxStatsEntryRepository */
    protected $fluxStatsEntryRepository;

    /**
     * Sparkline constructor.
     *
     * @param EntityManager        $em
     * @param AuthorizationChecker $authorizationChecker
     *
     * @internal param string $dateFormat
     */
    public function __construct(EntityManager $em, AuthorizationChecker $authorizationChecker,
                                TokenStorageInterface $tokenStorage,
                                FluxStatsEntryRepository $fluxStatsEntryRepository
    ) {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->fluxStatsEntryRepository = $fluxStatsEntryRepository;
    }

    /**
     * @param Flux|null $flux
     * @param int       $maxDays
     * @param string    $dateFormat
     *
     * @return array
     */
    public function getSparklineData(Flux $flux = null, int $maxDays = 10, string $dateFormat = 'Y/m/d'): array
    {
        return $this->getData(self::SPARKLINE, $flux, $maxDays, $dateFormat);
    }

    /**
     * @param Flux|null $flux
     * @param int       $maxDays
     * @param string    $dateFormat
     *
     * @return array
     */
    public function getMorrisData(Flux $flux = null, int $maxDays = 10, string $dateFormat = 'Y-m-d'): array
    {
        return $this->getData(self::MORRIS, $flux, $maxDays, $dateFormat);
    }

    /**
     * Central method.
     *
     * @param int       $type
     * @param Flux|null $flux
     * @param int       $maxDays
     * @param string    $dateFormat
     *
     * @return array
     */
    public function getData(int $type, Flux $flux = null, int $maxDays = 10, string $dateFormat = 'Y/m/d'): array
    {
        $data = $this->getRawData($flux, $maxDays, $dateFormat);

        switch ($type) {
            case self::SPARKLINE:
                return $this->formatDataForSparkline($data);
            case self::MORRIS:
                return $this->formatDataForMorris($data);
            default:
                throw new \LogicException('Use of inexistant type.');
        }
    }

    /**
     * @param Flux|null $flux
     * @param int       $maxDays
     * @param string    $dateFormat
     *
     * @return array
     */
    protected function getRawData(Flux $flux = null, int $maxDays, string $dateFormat): array
    {
        $entriesRepository = $this->em->getRepository(FluxStatsEntry::class);
        /** @var FluxStatsEntry[] $statsEntries */
        $statsEntries = $entriesRepository->findForLastDays($maxDays, $flux);

        $sortedEntries = [];
        foreach ($statsEntries as $statsEntry) {
            $dateStr = $statsEntry->getDate()->format($dateFormat);
            $sortedEntries[$dateStr][] = $statsEntry;
        }

        $data = [];
        foreach ($sortedEntries as $dateStr => $entries) {
            $data[$dateStr] = array_sum(array_map(function (FluxStatsEntry $entry): int {
                return $entry->getNbPublishedRdfResources();
            }, $entries));
        }

        return array_reverse($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function formatDataForSparkline(array $data): array
    {
        return [
            'published_sum' => end($data),
            'published_graph' => $data,
        ];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function formatDataForMorris(array $data): array
    {
        return [
            'published_sum' => end($data),
            'published_graph' => (function () use (&$data): array {
                $graph = [];
                foreach ($data as $day => $total) {
                    $graph[] = [
                        'date' => $day,
                        'value' => $total,
                    ];
                }

                return $graph;
            })(),
        ];
    }
}
