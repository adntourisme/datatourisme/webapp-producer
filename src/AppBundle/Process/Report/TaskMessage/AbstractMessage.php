<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\Process;
use Doctrine\ORM\EntityManager;

abstract class AbstractMessage
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Process
     */
    protected $process;

    /**
     * AbstractMessage constructor.
     *
     * @param $process
     * @param $em
     */
    public function __construct(EntityManager $em, Process $process)
    {
        $this->em = $em;
        $this->process = $process;
    }

    /**
     * batch execute to handle memory during multiple entity creation.
     */
    protected function batchProcess($iterator, $callback, $batchSize = 1000)
    {
        $i = 0;
        foreach ($iterator as $item) {
            ++$i;
            $callback($item);
            if (($i % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->reload();
            }
        }
        $this->em->flush();
        $this->em->clear();
        $this->reload();
        //gc_collect_cycles();
    }

    /**
     * reload process from entity manager.
     */
    protected function reload()
    {
        $this->process = $this->em->getRepository(Process::class)->find($this->process->getId());
    }
}
