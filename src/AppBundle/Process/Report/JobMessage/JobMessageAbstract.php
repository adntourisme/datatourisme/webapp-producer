<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\JobMessage;

abstract class JobMessageAbstract
{
    public function __construct($data)
    {
        $this->stats = new \ArrayObject();
        isset($data['uuid']) ? $this->setUuid($data['uuid']) : '';
        isset($data['stats']) ? $this->setStats($data['stats']) : '';
        isset($data['task']) ? $this->setTask($data['task']) : '';
    }

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     */
    private $task;

    /**
     * @var \ArrayObject
     */
    private $stats;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * @param mixed $stats
     */
    public function setStats($stats)
    {
        foreach ($stats as $key => $stat) {
            $this->stats->offsetSet($key, $stat);
        }
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }
}
