<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Notification;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;

class FluxSubmitType extends AbstractType
{
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    /**
     * @return Flux
     */
    public function getSubject()
    {
        return parent::getSubject();
    }

    public function getMessage()
    {
        return 'Le flux {{ flux.name }} est en attente de validation';
    }

    public function getContext()
    {
        return array('flux' => $this->getSubject());
    }

    public function getRoute()
    {
        return  'flux.detail';
    }

    public function getRouteParameters()
    {
        return ['id' => $this->getSubject()->getId()];
    }

    public function getLevel()
    {
        return Logger::INFO;
    }

    public function getRecipients()
    {
        $recipients = $this->_em->getRepository('AppBundle:User')->findBy(['role' => ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']]);

        return $recipients;
    }

    public function getEntity()
    {
        $notification = new Notification();
        $notification->setRole('ROLE_ADMIN');
        $notification->setRoute('flux.detail');
        $notification->setRouteParams(['id' => $this->getSubject()->getId()]);

        return $notification;
    }
}
