<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Notification;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Monolog\Logger;

class FluxInvalidateType extends AbstractType
{
    /**
     * @return Flux
     */
    public function getSubject()
    {
        return parent::getSubject();
    }

    public function getMessage()
    {
        return "Le flux {{ flux.name }} n'a pas été validé";
    }

    public function getDescription()
    {
        return $this->getSubject()->getReasonInvalidate();
    }

    public function getContext()
    {
        return array('flux' => $this->getSubject());
    }

    public function getRoute()
    {
        return  'flux.detail';
    }

    public function getRouteParameters()
    {
        return ['id' => $this->getSubject()->getId()];
    }

    public function getLevel()
    {
        return Logger::WARNING;
    }

    public function getRecipients()
    {
        $recipients = $this->getSubject()->getOrganization()->getUsers();

        return $recipients;
    }

    public function getEntity()
    {
        $notification = new Notification();
        $notification->setOrganization($this->getSubject()->getOrganization());

        return $notification;
    }
}
