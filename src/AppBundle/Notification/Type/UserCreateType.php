<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Monolog\Logger;

class UserCreateType extends AbstractType
{
    /**
     * @return User
     */
    public function getSubject()
    {
        return parent::getSubject();
    }

    public function getMessage()
    {
        return "L'utilisateur {{ user.fullName }} vient de rejoindre votre structure !";
    }

    public function getContext()
    {
        return array('user' => $this->getSubject());
    }

    public function getLevel()
    {
        return Logger::INFO;
    }

    public function getRecipients()
    {
        if ($this->getSubject()->getOrganization()) {
            $recipients = $this->getSubject()->getOrganization()->getUsers();
            $recipients->removeElement($this->getSubject());

            return $recipients;
        }

        return null;
    }

    public function getEntity()
    {
        if ($this->getSubject()->getOrganization()) {
            $notification = new Notification();
            $notification->setOrganization($this->getSubject()->getOrganization());

            return $notification;
        }

        return null;
    }
}
