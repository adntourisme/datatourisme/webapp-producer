<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Process;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Doctrine\Common\Collections\Collection;
use Monolog\Logger;

class ProcessSuccessType extends AbstractType
{
    /**
     * @return Process
     */
    public function getSubject(): Process
    {
        return parent::getSubject();
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'Le flux {{ process.flux.name }} a terminé son traitement.';
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        $process = $this->getSubject();

        return array(
            'process' => $process,
            'flux' => $process->getFlux(),
        );
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return 'flux.detail';
    }

    /**
     * @return array
     */
    public function getRouteParameters(): array
    {
        return ['id' => $this->getSubject()->getFlux()->getId()];
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return Logger::NOTICE;
    }

    /**
     * @return Collection
     */
    public function getRecipients(): Collection
    {
        return $this->getSubject()->getFlux()->getOrganization()->getUsers();
    }

    /**
     * @return Notification
     */
    public function getEntity(): Notification
    {
        $notification = new Notification();
        $notification->setOrganization(
            $this->getSubject()->getFlux()->getOrganization()
        );

        return $notification;
    }
}
