<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class AlignmentRulesetRevisionVoter.
 */
class AlignmentRulesetRevisionVoter extends AbstractVoter
{
    const CREATE = 'alignment_ruleset_revision.create';
    const EDIT = 'alignment_ruleset_revision.edit';
    const DELETE = 'alignment_ruleset_revision.delete';
    const SUBMIT = 'alignment_ruleset_revision.submit';
    const UNSUBMIT = 'alignment_ruleset_revision.unsubmit';
    const VALIDATE = 'alignment_ruleset_revision.validate';
    const INVALIDATE = 'alignment_ruleset_revision.invalidate';
    const ARCHIVE = 'alignment_ruleset_revision.archive';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        $reflection = new \ReflectionClass($this);
        $grantings = array_diff($reflection->getConstants(), $reflection->getParentClass()->getConstants());
        if (!in_array($attribute, $grantings, true)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User && !is_string($user)) {
            return false;
        }

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($user, $subject);
            case self::EDIT:
                return $this->canEdit($user, $subject);
            case self::DELETE:
                return $this->canDelete($user, $subject);
            case self::SUBMIT:
                return $this->canSubmit($user, $subject);
            case self::UNSUBMIT:
                return $this->canUnsubmit($user, $subject);
            case self::VALIDATE:
                return $this->canValidate($user);
            case self::INVALIDATE:
                return $this->canInvalidate($user);
            case self::ARCHIVE:
                return $this->canArchive($user);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canCreate(User $user, Flux $flux): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        if ($user->getOrganization()->getId() !== $flux->getOrganization()->getId()) {
            return false;
        }

        return true;
    }

    /**
     * @param User                     $user
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    private function canEdit(User $user, AlignmentRulesetRevision $revision): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        $flux = $revision->getAlignmentRuleset()->getFlux()[0];
        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User                     $user
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    private function canDelete(User $user, AlignmentRulesetRevision $revision): bool
    {
        return $this->canEdit($user, $revision);
    }

    /**
     * @param User                     $user
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    private function canSubmit(User $user, AlignmentRulesetRevision $revision): bool
    {
        return $this->canEdit($user, $revision);
    }

    /**
     * @param User                     $user
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    private function canUnsubmit(User $user, AlignmentRulesetRevision $revision): bool
    {
        return $this->canSubmit($user, $revision);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canValidate(User $user): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canInvalidate(User $user): bool
    {
        return $this->canValidate($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canArchive(User $user): bool
    {
        return $this->canValidate($user);
    }
}
