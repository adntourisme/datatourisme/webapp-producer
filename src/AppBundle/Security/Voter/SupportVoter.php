<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\Organization;
use AppBundle\Entity\SupportThread;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class SupportVoter.
 */
class SupportVoter extends AbstractVoter
{
    const SEE_ADMIN = 'support.see.admin';
    const SEE_STRUCTURE = 'support.see.structure';
    const SEE_ALL_THREADS = 'support.see.all_threads';
    const SEE_THREAD = 'support.see.thread';
    const COUNT_UNREAD_THREADS = 'support.count.unread_threads';
    const ADD_THREAD = 'support.add.thread';
    const ADD_POST = 'support.add.post';
    const SELECT_THREAD_ORGANIZATION = 'support.select.thread.organization';

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::SEE_ADMIN:
                return $this->canSeeAdmin($user);
            case self::SEE_STRUCTURE:
                return $this->canSeeStructure($user);
            case self::SEE_ALL_THREADS:
                return $this->canSeeAllThreads($user);
            case self::SEE_THREAD:
                return $this->canSeeThread($user, $subject);
            case self::COUNT_UNREAD_THREADS:
                return $this->canCountUnreadThreads($user, $subject);
            case self::ADD_THREAD:
                return $this->canAddThread($user, $subject);
            case self::SELECT_THREAD_ORGANIZATION:
                return $this->canSelectThreadOrganization($user);
            case self::ADD_POST:
                return $this->canAddPost($user, $subject, $token);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canSeeAdmin(User $user): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canSeeStructure(User $user): bool
    {
        return $this->canSeeAdmin($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canSeeAllThreads(User $user): bool
    {
        return $this->canSeeAdmin($user);
    }

    /**
     * @param User          $user
     * @param SupportThread $thread
     *
     * @return bool
     */
    private function canSeeThread(User $user, SupportThread $thread): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        if ($user->getOrganization()->getId() === $thread->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    private function canCountUnreadThreads(User $user, User $subject): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        if ($user->getId() === $subject->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User              $user
     * @param Organization|null $organization
     *
     * @return bool
     */
    private function canAddThread(User $user, Organization $organization = null): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        if ($organization instanceof Organization && $organization->getId() === $user->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function canSelectThreadOrganization(User $user): bool
    {
        return $this->canSeeAdmin($user);
    }

    /**
     * @param User           $user
     * @param SupportThread  $thread
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canAddPost(User $user, SupportThread $thread, TokenInterface $token): bool
    {
        if ($this->canSeeThread($user, $thread) === true && $this->isImpersonated($token) === false) {
            return true;
        }

        return false;
    }
}
