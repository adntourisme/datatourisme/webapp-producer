<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class FluxVoter.
 */
class FluxVoter extends AbstractVoter
{
    const SEE = 'flux.see';
    const CREATE = 'flux.create';
    const EDIT = 'flux.edit';
    const PREVIEW = 'flux.preview';
    const START = 'flux.start';
    const SEE_RESOURCES = 'flux.see.resources';
    const SEE_ANOMALIES = 'flux.see.anomalies';
    const FILTER_ORGANIZATION = 'flux.filter.organization';
    const PAUSE = 'flux.pause';
    const UNPAUSE = 'flux.unpause';
    const UNBLOCK = 'flux.unblock';
    const BLOCK = 'flux.block';
    const DELETE = 'flux.delete';

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // specific case : anonymous
        if (is_string($user) && $attribute == self::START) {
            return $this->canStart($user, $subject);
        }

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::SEE:
                return $this->canSee($user, $subject);
            case self::CREATE:
                return $this->canCreate($user);
            case self::EDIT:
                return $this->canEdit($user, $subject);
            case self::PREVIEW:
                return $this->canPreview($user, $subject);
            case self::START:
                return $this->canStart($user, $subject);
            case self::SEE_RESOURCES:
                return $this->canSeeResources($user, $subject);
            case self::SEE_ANOMALIES:
                return $this->canSeeAnomalies($user, $subject);
            case self::FILTER_ORGANIZATION:
                return $this->canFilterOrganization($user);
            case self::PAUSE:
                return $this->canPause($user, $subject);
            case self::UNPAUSE:
                return $this->canUnPause($user, $subject);
            case self::BLOCK:
                return $this->canBlock($user, $subject);
            case self::UNBLOCK:
                return $this->canUnBlock($user, $subject);
            case self::DELETE:
                return $this->canDelete($user, $subject);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canSee(User $user, Flux $flux): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canCreate(User $user): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canEdit(User $user, Flux $flux): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canPreview(User $user, Flux $flux): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return $this->canEdit($user, $flux);
    }

    /**
     * @param $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canStart($user, Flux $flux): bool
    {
        if ($flux) {
            $isProduction = $flux->getStatus() === Flux::STATUS_PRODUCTION;
            $isDraft = $flux->getStatus() === Flux::STATUS_DRAFT;
            $draft = $flux->getAlignmentRuleset() ? $flux->getAlignmentRuleset()->getDraft() : null;
            $isAnonymous = is_string($user);

            // case : the flux is in first draft, user can upload a new example file
            if (!$isAnonymous && $isDraft && $flux->getMode() === Flux::MODE_MANUAL
                && $draft && $draft->getStatus() == AlignmentRulesetRevision::STATUS_DRAFT) {
                return $this->canEdit($user, $flux);
            }

            // if in prod and not already
            if ($isProduction && !$flux->isActive()) {
                // anonymous start (push)
                if ($flux->getMode() === Flux::MODE_PUSH && $isAnonymous) {
                    return true;
                }

                return $this->canEdit($user, $flux);
            }
        }

        return false;
    }

    /**
     * @param User    $user
     * @param Process $process
     *
     * @return bool
     */
    private function canSeeResources(User $user, Process $process): bool
    {
        $flux = $process->getFlux();
        if ($flux->getStatus() === Flux::STATUS_PRODUCTION && $process->getStatus() === Process::STATUS_RUNNING) {
            return false;
        }

        return $this->canSee($user, $flux);
    }

    /**
     * @param User    $user
     * @param Process $process
     *
     * @return bool
     */
    private function canSeeAnomalies(User $user, Process $process): bool
    {
        return $this->canSeeResources($user, $process);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canFilterOrganization(User $user)
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canPause(User $user, Flux $flux)
    {
        if ($flux->getStatus() != Flux::STATUS_PRODUCTION) {
            return false;
        }
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }
        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canUnPause(User $user, Flux $flux)
    {
        if ($flux->getStatus() != Flux::STATUS_PAUSED) {
            return false;
        }
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }
        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canBlock(User $user, Flux $flux)
    {
        if ($this->hasRole('ROLE_ADMIN', $user) &&
            $flux->getStatus() == Flux::STATUS_PRODUCTION) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canUnBlock(User $user, Flux $flux)
    {
        if ($this->hasRole('ROLE_ADMIN', $user) &&
            $flux->getStatus() == Flux::STATUS_BLOCKED) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Flux $flux
     *
     * @return bool
     */
    private function canDelete(User $user, Flux $flux)
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }
        if ($user->getOrganization()->getId() === $flux->getOrganization()->getId() && $flux->getStatus() == Flux::STATUS_DRAFT) {
            return true;
        }

        return false;
    }
}
