<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\Faq;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class FaqVoter.
 */
class FaqVoter extends AbstractVoter
{
    const EDIT = 'faq.edit';
    const ADD = 'faq.add';
    const REMOVE = 'faq.remove';
    const REORDER = 'faq.reorder';

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user);
            case self::ADD:
                return $this->canAdd($user);
            case self::REMOVE:
                return $this->canDelete($user);
            case self::REORDER:
                return $this->canReorder($user);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canEdit(User $user): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canAdd(User $user): bool
    {
        return $this->canEdit($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canDelete(User $user): bool
    {
        return $this->canEdit($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function canReorder(User $user): bool
    {
        return $this->canEdit($user);
    }
}
