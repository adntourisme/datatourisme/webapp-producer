<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class PreventBlockAccountException.
 */
class PreventBlockAccountException extends AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        if (5 - $this->getToken()->getUser()->getFailLoginNumberAttempts() > 1) {
            return 'Invalid credentials. Your account will be blocked for an hour after five failed connection attempts. You have nbr_attempt_left attempts left.';
        }

        return 'Invalid credentials. Your account will be blocked for an hour after five failed connection attempts. You have one attempt left.';
    }

    /**
     * {@inheritdoc}
     */
    public function getMessageData()
    {
        return array('nbr_attempt_left' => 5 - $this->getToken()->getUser()->getFailLoginNumberAttempts());
    }
}
