<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Stats;

use AppBundle\Entity\Flux;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatsFluxUpdateCommand extends ContainerAwareCommand
{
    const DAY_FORMAT = 'Y/m/d';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $dayFormat = self::DAY_FORMAT;

        $todayStr = (new \DateTime())->format($dayFormat);

        $this
            ->setName('datatourisme:stats:flux:update')
            ->setAliases(['stats:flux:update']) // legacy
            ->setDescription("Updates Flux' stats")
            ->setHelp("This command updates Flux' stats")
            ->addOption(
                'day', 'd', InputOption::VALUE_REQUIRED,
                "Date to be updated.\n".
                    "Format: {$dayFormat}\n".
                    "Default: {$todayStr} (today)",
                $todayStr
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dayArg = $input->getOption('day');
        $dayDate = \DateTime::createFromFormat(self::DAY_FORMAT, $dayArg);

        if ($dayDate->getTimestamp() > time()) {
            throw new \InvalidArgumentException('Cannot process an upcoming date. Need a TARDIS for that.');
        }

        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $statsEntryManager = $container->get('app.utils.stats_entry_manager');

        foreach ($em->getRepository(Flux::class)->findAllLivingOnDate($dayDate) as $flux) {
            $entry = $statsEntryManager->createFluxStatsEntry($flux, $dayDate, true, false);
            $em->persist($entry);
        }
        $uw = $em->getUnitOfWork();
        $uw->computeChangeSets();
        $nbInserts = count($uw->getScheduledEntityInsertions());
        $nbUpdates = count($uw->getScheduledEntityUpdates());

        $em->flush();

        $this->handleOutput($output, $dayArg, $nbInserts, $nbUpdates);
    }

    /**
     * Handles console output.
     *
     * @param OutputInterface $output
     * @param string          $dayArg
     * @param int             $nbInserts
     * @param int             $nbUpdates
     *
     * @return bool
     */
    private function handleOutput(OutputInterface $output, string $dayArg, int $nbInserts, int $nbUpdates): bool
    {
        if ($output->getVerbosity() <= OutputInterface::VERBOSITY_QUIET) {
            return false;
        }

        $e = chr(27);
        $bold = "{$e}[1m";
        $green = "{$e}[92m";
        $yellow = "{$e}[93m";
        $blue = "{$e}[94m";
        $endf = "{$e}[0m";
        $space = '    ';
        $sep = '  |  ';

        if ($nbInserts === 0 && $nbUpdates === 0) {
            $output->writeln("No entries to be updated on {$bold}$dayArg{$endf}.");
        } else {
            $s = function (int $nb): string { return $nb > 1 ? 's' : ''; };
            $output->writeln(
                "{$bold}{$yellow}[$dayArg]{$endf}".
                "{$space}".
                "{$bold}{$green}$nbInserts{$endf} insertion{$s($nbInserts)}".
                "{$sep}".
                "{$bold}{$blue}$nbUpdates{$endf} update{$s($nbUpdates)}"
            );
        }

        return true;
    }
}
