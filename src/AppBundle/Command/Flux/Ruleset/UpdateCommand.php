<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Flux\Ruleset;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:flux:ruleset:update');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $revisions = $em->getRepository(AlignmentRulesetRevision::class)->findAll();
        /* @var AlignmentRulesetRevision $ruleset */
        foreach ($revisions as $revision) {
            $content = $revision->getContent();

            $rules = $content['rules'];
            $keys = array_keys($rules);
            usort($keys, function($a, $b) {
                if(count(explode("/", $a)) > count(explode("/", $b))) {
                    return 1;
                } elseif (count(explode("/", $a)) < count(explode("/", $b))) {
                    return -1;
                }
                return 0;
            });

            $rules = [];
            foreach($keys as $key) {
                $parts = explode("/", $key);
                $target = &$rules;

                while(count($parts) > 1) {
                    $part = array_shift($parts);
                    if(!isset($target[$part]['rules'])) {
                        $target[$part]['rules'] = [];
                    }
                    $target = &$target[$part]['rules'];
                }

                $part = array_shift($parts);
                $target[$part] = $content['rules'][$key];
            }

            $content['rules'] = unserialize(serialize($rules));
            $revision->setContent($content);
            $em->flush();
        }
    }
}