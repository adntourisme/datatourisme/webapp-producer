<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Flux\Ruleset;

use AppBundle\Entity\Flux;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DumpCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:flux:ruleset:dump')
            ->addArgument('flux_id', InputArgument::REQUIRED, 'flux id')
            ->addArgument('revision_id', InputArgument::OPTIONAL, 'ruleset revision id (default: active)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fluxId = $input->getArgument('flux_id');
        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var Flux $flux */
        $flux = $em->getRepository(Flux::class)->find($fluxId);
        if (!$flux) {
            throw new InvalidArgumentException("Le flux $fluxId est introuvable");
        }

        $ruleset = null;

        if ($input->getArgument('revision_id')) {
            $revisionId = $input->getArgument('revision_id');
            foreach ($flux->getAlignmentRuleset()->getRevisions() as $revision) {
                if ($revision->getId() == $revisionId) {
                    $ruleset = $revision;
                    break;
                }
            }
        } else {
            $ruleset = $flux->getAlignmentRuleset()->getActive();
            if (!$ruleset) {
                $ruleset = $flux->getAlignmentRuleset()->getDraft();
            }
        }

        if ($ruleset == null) {
            throw new InvalidArgumentException('La révision est introuvable');
        }

        $output->writeln(json_encode($ruleset->getContent(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}
