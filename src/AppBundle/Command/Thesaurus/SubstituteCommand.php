<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Thesaurus;

use AppBundle\Entity\Organization;
use AppBundle\Entity\ThesaurusAlignment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SubstituteCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:thesaurus:substitute')
            ->addArgument('old_uri', InputArgument::REQUIRED, 'old concept uri')
            ->addArgument('new_uri', InputArgument::REQUIRED, 'new concept uri');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oldUri = $input->getArgument('old_uri');
        $newUri = $input->getArgument('new_uri');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository(ThesaurusAlignment::class);

        $alignments = $repository->findByIri($oldUri);

        /** @var ThesaurusAlignment $alignment */
        foreach($alignments as $alignment) {
            $output->writeln("ThesaurusAlignment : " . $alignment->getIri());
            /** @var ThesaurusAlignment $other */
            $other = $repository->findOneBy(['organization' => $alignment->getOrganization(), 'iri' => $newUri]);
            if(!$other) {
                $other = new ThesaurusAlignment($alignment->getOrganization(), $newUri, $alignment->getValues());
                $em->persist($other);
            } else {
                $other->addValues($alignment->getValues());
            }

            $em->remove($alignment);
        }

        $em->flush();
    }
}
