<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Reuse;

use AppBundle\Entity\RdfResource;
use Doctrine\DBAL\Connection;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ComputeCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:reuse:compute')
            ->setAliases(['datatourisme:reuse:sync'])
            ->setDescription('Compute short term reuse stats and store it in database');
    }

    /**
     * getFluxIdSubSelect
     *
     * @return void
     */
    private function getFluxIdSubSelect() {
        return 'SELECT DISTINCT flux_id FROM download d WHERE d.created_at > (CURRENT_DATE - INTERVAL \'1 month\')';
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Prepare entity manager and connections
        $emProducer = $this->getContainer()->get('doctrine')->getManager();
        $emConsumer = $this->getContainer()->get('doctrine')->getManager('consumer');
        /** @var Connection $conProducer */
        $conProducer = $emProducer->getConnection();
        /** @var Connection $conConsumer */
        $conConsumer = $emConsumer->getConnection();

        $progressBar = new ProgressBar($output, 5);
        $progressBar->setFormatDefinition('custom', ' %current%/%max% -- %message%');
        $progressBar->setMessage('Start...');
        $progressBar->setFormat('custom');
        $progressBar->start();

        // prepare temporary table
        $this->buildResourceTable($progressBar, $conConsumer, $conProducer);
        $this->buildFluxTable($progressBar, $conConsumer, $conProducer);

        // fill the base
        $progressBar->setMessage('Fill reuse database...');
        $progressBar->advance();

        $emProducer->getConnection()->beginTransaction();
        try {
            $sql = 'TRUNCATE reuse';
            $conProducer->executeQuery($sql);

            // todo : make this feature live !
            // $sql = 'ALTER SEQUENCE reuse_id_seq RESTART WITH 1';
            // $conProducer->executeQuery($sql);

            $sql = 'INSERT INTO reuse (poi_nb, download_at, download_type, app_consumer_id, app_id, app_name, app_desc, app_url, consumer_id, consumer_name, consumer_url, consumer_type, consumer_zip, consumer_country, poi_type, poi_type2, poi_creator, organization_id)
                       (SELECT count(*),
                            download_at,
                            download_type,
                            CONCAT(user_id, \'-\', app_id) as app_consumer_id,
                            app_id,
                            app_name,
                            app_desc,
                            app_url,
                            user_id,
                            COALESCE(user_org, user_name),
                            user_url,
                            user_type,
                            user_zip,
                            user_country,
                            poi.type,
                            poi.type2,
                            poi.creator,
                            flux.organization_id
                       FROM _reuse_tmp_resource tmp_res
                       INNER JOIN _reuse_tmp_flux tmp_flux ON tmp_res.flux_id = tmp_flux.flux_id
                       INNER JOIN rdf_resource poi ON tmp_res.uri = poi.uri AND poi.status = :status
                       INNER JOIN flux ON flux.id = poi.flux_id
                      GROUP BY download_at, download_type, app_consumer_id, app_id, app_name, app_desc, app_url, user_id, user_org, user_name, user_url, user_type, user_zip, user_country, poi.type, poi.type2, poi.creator, flux.organization_id)
                      ';
            $conProducer->executeQuery($sql, ["status" => RdfResource::STATUS_PUBLISHED]);
            $emProducer->getConnection()->commit();
            $progressBar->finish();
            $output->writeln('');
        } catch (Exception $e) {
            $output->writeln("An error while inserting in table 'reuse' occurred. We rollback the database... ");
            $output->writeln($e->getMessage());
            $emProducer->getConnection()->rollBack();
            $output->writeln('done ! ');
        }
    }

    /**
     * @param ProgressBar $progress
     * @param Connection $conConsumer
     * @param Connection $conProducer
     */
    private function buildResourceTable(ProgressBar $progressBar, Connection $conConsumer, Connection $conProducer)
    {
        $selectSQL = 'SELECT DISTINCT 
            r.uri, 
            r.flux_id, 
            d.created_at as download_at, 
            d.type as download_type, 
            a.id as app_id,
            a.name as app_name,
            a.description as app_desc,
            a.url as app_url
          FROM flux f
          INNER JOIN download d ON d.id = f.last_download_id
          INNER JOIN application a ON d.application_id = a.id
          INNER JOIN rdf_resource r ON r.flux_id = f.id
          WHERE f.id IN ('. $this->getFluxIdSubSelect().')';
          
        $createSQL = [
            'CREATE TEMPORARY TABLE _reuse_tmp_resource (uri VARCHAR(511), flux_id INTEGER, download_at TIMESTAMP, download_type VARCHAR(16), app_id INTEGER, app_name VARCHAR(255), app_desc TEXT, app_url VARCHAR(255))',
            'CREATE INDEX IDX_reuse_uri ON _reuse_tmp_resource (uri)',
            'CREATE INDEX IDX_reuse_app_id ON _reuse_tmp_resource (app_id)'
        ];

        $this->buildTemporaryTable($progressBar, $conConsumer, $conProducer, "_reuse_tmp_resource", $selectSQL, $createSQL);
    }

    /**
     * @param ProgressBar $progress
     * @param Connection $conConsumer
     * @param Connection $conProducer
     */
    private function buildFluxTable(ProgressBar $progressBar, Connection $conConsumer, Connection $conProducer)
    {
        $selectSQL = 'SELECT
                f.id as flux_id,
                u.id as user_id,
                TRIM(u.first_name || \' \' || u.last_name) as user_name,
                u.organization as user_org,
                ut.name as user_type,
                u.website as user_url,
                u.zip as user_zip,
                u.country as user_country
            FROM flux f 
            INNER JOIN _user u ON u.id = f.user_id
            LEFT JOIN user_type ut ON ut.id = u.type_id
            WHERE f.id IN ('. $this->getFluxIdSubSelect().')
        ';

        $createSQL = [
            'CREATE TEMPORARY TABLE _reuse_tmp_flux (
                flux_id INTEGER,
                user_id INTEGER,
                user_name VARCHAR(255),
                user_org VARCHAR(255),
                user_type VARCHAR(255),
                user_url VARCHAR(255),
                user_zip VARCHAR(128),
                user_country VARCHAR(2)
             )',
            'CREATE UNIQUE INDEX IDX_reuse_tmp_flux_app_id ON _reuse_tmp_flux (flux_id)',
            'CREATE INDEX IDX_reuse_tmp_flux_user_id ON _reuse_tmp_flux (user_id)'
        ];

        $this->buildTemporaryTable($progressBar, $conConsumer, $conProducer, "_reuse_tmp_flux", $selectSQL, $createSQL);
    }


    /**
     * @param $select
     * @param $create
     */
    private function buildTemporaryTable(ProgressBar $progressBar, Connection $conConsumer, Connection $conProducer, $table, $select, $create) {
        $tmpfname = tempnam(sys_get_temp_dir(), '_reuse_tmp');

        $progressBar->setMessage('Collect data from consumer database...');
        $progressBar->advance();

        $pdo = $conConsumer->getWrappedConnection();
        $pdo->pgsqlCopyToFile("($select)", $tmpfname);

        $progressBar->setMessage('Create temporary table...');
        $progressBar->advance();

        foreach($create as $sql) {
            $conProducer->query($sql);
        }

        $pdo = $conProducer->getWrappedConnection();
        $pdo->pgsqlCopyFromFile($table, $tmpfname);

        unlink($tmpfname);
    }
}
