<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Organization;
use AppBundle\Entity\ThesaurusAlignment;
use ClassesWithParents\D;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateOntologyURICommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:update-ontology-uri')
            ->addArgument('old', InputArgument::REQUIRED, 'old uri')
            ->addArgument('new', InputArgument::REQUIRED, 'new uri');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oldUri = $input->getArgument('old');
        $newUri = $input->getArgument('new');
        $em = $this->getContainer()->get('doctrine')->getManager();

        // update alignement revisions
        $revisions = $em->getRepository(AlignmentRulesetRevision::class)->findAll();
        /* @var AlignmentRulesetRevision $revision */
        foreach ($revisions as $revision) {
            $output->writeln("AlignmentRulesetRevision : " . $revision->getAlignmentRuleset()->getId());
            $content = $revision->getContent();
            $json = json_encode($content);
            $json = preg_replace("/".preg_quote( "\"" . str_replace('/', '\/', $oldUri), "/")."/",
                "\"" . str_replace('/', '\/', $newUri),
                $json);
            $content = json_decode($json, true);
            $revision->setContent($content);
        }
        $em->flush();

        // update thesaurus revisions
        $alignements = $em->getRepository(ThesaurusAlignment::class)->findAll();
        /* @var ThesaurusAlignment $alignement */
        foreach ($alignements as $alignement) {
            $output->writeln("ThesaurusAlignment : " . $alignement->getId());
            if(strpos($alignement->getIri(), $oldUri) === 0) {
                $alignement->setIri(str_replace($oldUri, $newUri, $alignement->getIri()));
            }
        }
        $em->flush();
    }
}
