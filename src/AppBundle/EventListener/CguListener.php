<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CguListener
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * LastLoginListener constructor.
     *
     * @param Router               $router
     * @param TokenStorage         $tokenStorage
     * @param RequestStack         $requestStack
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(Router $router, TokenStorage $tokenStorage, RequestStack $requestStack, AuthorizationChecker $authorizationChecker)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Redirect to CGU page if CGU are not validated for User Organization.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $token = $this->tokenStorage->getToken();
        if ($token) {
            $user = $token->getUser();
            if ($user && $user instanceof User && $user->getOrganization() && $user->getOrganization()->isCgu() === false) {
                $request = $this->requestStack->getCurrentRequest();
                $route = $request->get('_route');
                if (0 !== strpos($route, 'security.') && $this->authorizationChecker->isGranted('ROLE_USER', $user)) {
                    $redirectUrl = $this->router->generate('security.cgu');
                    $response = new RedirectResponse($redirectUrl);
                    $event->setResponse($response);
                }
            }
        }
    }
}
