<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\Security\Exception\PreventBlockAccountException;
use AppBundle\Security\Exception\TemporarilyBlockedAccountException;
use Datatourisme\Bundle\WebAppBundle\Mailer\Mailer;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Sylius\Component\Mailer\Sender\Sender;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Class LastLoginListener.
 */
class LoginSubscriber implements EventSubscriberInterface
{
    /**
     * @param Registry $registry
     */
    protected $registry;

    /**
     * @var Sender
     */
    protected $mailer;

    /**
     * LastLoginListener constructor.
     *
     * @param Registry $registry
     * @param Mailer   $mailer
     */
    public function __construct(Registry $registry, Mailer $mailer)
    {
        $this->registry = $registry;
        $this->mailer = $mailer;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        );
    }

    /**
     * Ban a user for an hour after five failed connection attempts.
     *
     * @param AuthenticationEvent $event
     */
    public function onAuthenticationFailure(AuthenticationEvent $event)
    {
        $username = $event->getAuthenticationToken()->getUser();
        if (is_string($username)) {
            $em = $this->registry->getManager();
            $user = $em->getRepository('AppBundle:User')->findOneBy(array('email' => $username));
            if ($user && $user instanceof User) {
                $now = new \DateTime();
                if ($user->getFailLoginDatetime() > $now) {
                    return;
                }

                // save login failure
                $accountIsBlocked = false;
                $user->setFailLoginNumberAttempts($user->getFailLoginNumberAttempts() + 1);
                if ($user->getFailLoginNumberAttempts() >= 5) {
                    $date = new \DateTime();
                    $date->add(new \DateInterval('PT60M'));
                    $user->setFailLoginDatetime($date);
                    $user->setFailLoginNumberAttempts(0);
                    $accountIsBlocked = true;
                }
                $this->registry->getManager()->flush();

                // return warning about the imminence of the account blockage on login form
                if (!$accountIsBlocked && $user->getFailLoginNumberAttempts() > 2 && $user->getFailLoginNumberAttempts() < 5) {
                    $ex = new PreventBlockAccountException('The presented password is invalid. Account will be blocked after 5 failed attempts');
                    $token = $event->getAuthenticationToken();
                    $token->setUser($user);
                    $ex->setToken($token);

                    throw $ex;
                }
                // alert the account is blocked on login form
                elseif ($accountIsBlocked) {
                    $ex = new TemporarilyBlockedAccountException('User account is blocked for an hour.');
                    $ex->setUser($user);
                    throw $ex;
                }
            }
        }
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        if ($user instanceof User) {
            $user->setFailLoginNumberAttempts(0);
            $user->addLoginHistory(new \DateTime());
            $em = $this->registry->getManager();
            $em->persist($user);
            $em->flush();
        }
    }

    /**
     * Impersonate event.
     *
     * @param SwitchUserEvent $event
     */
    public function onSwitchUser(SwitchUserEvent $event)
    {
        $targetUser = $event->getTargetUser();
        $sendEmail = $targetUser->isRole('ROLE_USER');
        $flash = sprintf('Vous êtes maintenant connecté en tant que <strong>%s</strong>.', $targetUser->__toString());
        if ($sendEmail) {
            $this->mailer->send('account.impersonated', $targetUser);
            $flash .= " Un email vient de lui être envoyé pour le notifier de l'opération.";
        }
        $event->getRequest()->getSession()->getFlashBag()->add('success', $flash);
    }
}
