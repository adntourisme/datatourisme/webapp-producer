<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Log;
use AppBundle\Entity\Process;
use AppBundle\Notification\Type\FluxValidateType;
use AppBundle\Notification\Type\ProcessErrorType;
use AppBundle\Notification\Type\ProcessSuccessType;
use AppBundle\Process\TaskRunner;
use Datatourisme\Bundle\WebAppBundle\Notification\NotificationDispatcher;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;

/**
 * Class WorkflowTransition.
 */
class WorkflowTransitionSubscriber implements EventSubscriberInterface
{
    /**
     * @var TaskRunner
     */
    protected $taskRunner;

    /**
     * @var NotificationDispatcher
     */
    protected $notifier;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var WorkflowRegistry
     */
    protected $workflowRegistry;

    /**
     * WorkflowTransition constructor.
     *
     * @param TaskRunner             $taskRunner
     * @param NotificationDispatcher $notifier
     * @param Registry               $registry
     * @param WorkflowRegistry       $workflowRegistry
     */
    public function __construct(TaskRunner $taskRunner, NotificationDispatcher $notifier, Registry $registry, WorkflowRegistry $workflowRegistry)
    {
        $this->taskRunner = $taskRunner;
        $this->notifier = $notifier;
        $this->registry = $registry;
        $this->workflowRegistry = $workflowRegistry;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            'workflow.flux_operation.transition.pause' => 'onFluxPause',
            'workflow.flux_operation.transition.unpause' => 'onFluxUnPause',
            'workflow.flux_operation.transition.block' => 'onFluxBlock',
            'workflow.flux_operation.transition.unblock' => 'onUnBlock',
            'workflow.process_operation.transition.error' => 'onProcessError',
            'workflow.process_operation.transition.success' => 'onProcessSuccess',
            'workflow.alignment_ruleset_revision_operation.transition.validate' => 'onRevisionValidate',
        );
    }

    /**
     * @param Event $event
     */
    public function onRevisionValidate(Event $event)
    {
        if (($revision = $event->getSubject()) instanceof AlignmentRulesetRevision) {
            $alignmentRuleset = $revision->getAlignmentRuleset();
            if (($formerActive = $alignmentRuleset->getActive()) instanceof AlignmentRulesetRevision) {
                $this->workflowRegistry
                    ->get($formerActive, 'alignment_ruleset_revision_operation')
                    ->apply($formerActive, 'archive');
            }

            /** @var Flux $flux */
            $flux = $alignmentRuleset->getFlux()[0];
            if ($flux->getStatus() === Flux::STATUS_DRAFT) {
                $this->workflowRegistry
                    ->get($flux, 'flux_operation')
                    ->apply($flux, 'validate');
            }
            $flux->setNewChecksum();
            $alignmentRuleset->setActiveVersion($revision->getVersion());
            $alignmentRuleset->setDraftVersion(null);

            $this->notifier->dispatch(FluxValidateType::class, $flux);
            $this->registry->getManager()->flush();
        }
    }

    /**
     * @param Event $event
     */
    public function onFluxPause(Event $event)
    {
        //$this->disableScheduleTask($event->getSubject());
    }

    /**
     * @param Event $event
     */
    public function onFluxUnPause(Event $event)
    {
        //$this->enableScheduleTask($event->getSubject());
    }

    /**
     * @param Event $event
     */
    public function onFluxBlock(Event $event)
    {
        //$this->disableScheduleTask($event->getSubject());
    }

    /**
     * @param Event $event
     */
    public function onUnBlock(Event $event)
    {
        //$this->enableScheduleTask($event->getSubject());
    }

    /**
     * @param Event $event
     */
    public function onProcessError(Event $event)
    {
        $process = $event->getSubject();
        if (!$process instanceof Process) {
            return;
        }

        $lastFatalLog = $this->registry->getManager()
            ->getRepository(Log::class)
            ->findOneBy(
                ['process' => $process, 'level' => Log::LOG_FATAL],
                ['createdAt' => 'desc', 'id' => 'desc']
            );

        $process->setReasonError($lastFatalLog);

        $this->notifier->dispatch(ProcessErrorType::class, $process);
    }

    /**
     * @param Event $event
     */
    public function onProcessSuccess(Event $event)
    {
        $process = $event->getSubject();
        if (!$process instanceof Process) {
            return;
        }

        $this->notifier->dispatch(ProcessSuccessType::class, $process);
    }

    /*
     * @param $entity
     * @deprecated : done now in doctrine listener
     */
//    protected function enableScheduleTask($entity)
//    {
//        if ($entity instanceof Flux) {
//            if ($entity->getMode() === Flux::MODE_PULL) {
//                $hour = $entity->getFluxCrawler()->getUpdateHour();
//                $this->taskManager->add($entity, $hour === null ? -1 : $hour);
//            } else {
//                $this->taskManager->delete($entity);
//            }
//        }
//    }

    /*
     * @param $entity
     * @deprecated : done now in doctrine listener
     */
//    protected function disableScheduleTask($entity)
//    {
//        if ($entity instanceof Flux) {
//            if ($entity->getMode() === Flux::MODE_PULL) {
//                $this->taskManager->disable($entity);
//            } else {
//                $this->taskManager->delete($entity);
//            }
//        }
//    }
}
