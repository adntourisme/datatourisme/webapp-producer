<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class StringToArrayTransformer.
 */
class ArrayToStringTransformer implements DataTransformerInterface
{
    protected $separator = "\r\n";

    /**
     * ArrayToStringTransformer constructor.
     *
     * @param string $separator
     */
    public function __construct(string $separator)
    {
        $this->separator = $separator;
    }

    /**
     * @param array|null $array
     *
     * @return string
     */
    public function transform($array = null): string
    {
        if ($array === null || (is_array($array) && empty($array))) {
            return '';
        }

        if (!is_array($array)) {
            $errorValue = gettype($array) !== 'object' ? gettype($array) : get_class($array);
            throw new \UnexpectedValueException("Array or null expected, $errorValue given.");
        }

        return implode($this->separator, $array);
    }

    /**
     * @param string|null $string
     *
     * @return array
     */
    public function reverseTransform($string = null): array
    {
        if ($string === null || (is_string($string) && empty(trim($string)))) {
            return [];
        }

        if (!is_string($string)) {
            $errorValue = gettype($string) !== 'object' ? gettype($string) : get_class($string);
            throw new \UnexpectedValueException("String or null expected, $errorValue given.");
        }

        $filterArray = function (array $array): array {
            // We filter duplicates (case insensitive way)
            $array = (function () use (&$array) {
                $lowered = array_map('strtolower', $array);

                return array_intersect_key($array, array_unique($lowered));
            })();

            // We filter dummy values (Examples: "...", "  ", "??!/", "(:", etc.)
            $array = array_filter($array, function ($val) {
                return preg_match("~^[\s{}()/\\,\.\!\?;:'\"\-_]*$~", $val) === 0;
            });

            // We trim remaining values
            $array = array_map('trim', $array);

            // We have to reindex array to avoid key holes
            return array_values($array);
        };

        return $filterArray(explode($this->separator, $string));
    }
}
