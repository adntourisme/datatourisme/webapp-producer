<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\SupportPost;
use AppBundle\Utils\MimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SupportPostType.
 */
class SupportPostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $allowedExtensions = MimeType::getExtensionsFromTypes(SupportPost::getAllowedMimeTypes());
        sort($allowedExtensions);

        $builder
            ->add('thread', SupportThreadType::class, array(
                'label' => false,
            ))
            ->add('content', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'rows' => '5',
                    'placeholder' => 'Votre message',
                ),
            ))
            ->add('file', FileType::class, array(
                'label' => 'Fichier joint',
                'required' => false,
                'help' => 'Le fichier joint est limité à 5Mo. <br><span class="text-sm">Extensions autorisées : '.implode(', ', $allowedExtensions).'</span>',
            ))
            ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))
            ->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $post = $event->getData();
        $thread = $post->getThread();
        $organization = $thread->getOrganization();
        if ($organization) {
            $form->get('thread')->get('organization')->setData($organization);
        }
        if ($thread->getId()) {
            $form->remove('thread');
        }
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $post = $event->getData();
        $thread = $post->getThread();
        if (!$thread->getOrganization()) {
            $thread->setOrganization($post->getUser()->getOrganization());
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => SupportPost::class,
            ));
    }
}
