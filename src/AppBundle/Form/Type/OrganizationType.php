<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Organization;
use AppBundle\Entity\OrganizationType as Type;
use Datatourisme\Bundle\WebAppBundle\Form\ImagePreviewType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrganizationType.
 */
class OrganizationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nom',
                'attr' => array(
                    'placeholder' => 'Nom de la structure',
                ),
            ))
            ->add('type', EntityType::class, array(
                'label' => 'Type',
                'class' => Type::class,
            ))
            ->add('address', TextareaType::class, array(
                'label' => 'Adresse',
                'attr' => array(
                    'placeholder' => 'Adresse',
                ),
            ))
            ->add('zip', TextType::class, array(
                'label' => 'Code postal',
                'attr' => array(
                    'placeholder' => 'Code postal',
                    'maxlength' => 5,
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'Ville',
                'attr' => array(
                    'placeholder' => 'Ville',
                ),
            ))
            ->add('phoneNumber', TextType::class, array(
                'label' => 'Téléphone',
                'attr' => array(
                    'placeholder' => 'Téléphone',
                ),
            ))
            ->add('website', UrlType::class, array(
                'label' => 'Adresse web',
                'attr' => array(
                    'placeholder' => 'Adresse web',
                ),
            ))
            ->add('file', ImagePreviewType::class, array(
                'label' => 'Logo',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Organization::class,
        ));
    }
}
