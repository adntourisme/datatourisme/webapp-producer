<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Notification\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class AccountParamsType extends AbstractType
{
    protected $security;

    /** 
     * __construct
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // @todo : use NotificationRegistry service
        $choices = [
            "Structure : nouveau membre" => Type\UserCreateType::class,
            "Structure : nouveau rapport de réutilisation" => Type\ReuseReportNotifyType::class,
            "Aide : nouvelle question de la FAQ" => Type\FaqCreateType::class,
            "Aide : nouvelle actualité" => Type\NewsCreateType::class,
            "Flux : validation acceptée" => Type\FluxValidateType::class,
            "Flux : validation refusée" => Type\FluxInvalidateType::class,
            "Flux : erreur fatale d'un traitement" => Type\ProcessErrorType::class,
            "Flux : succès d'un traitement" => Type\ProcessSuccessType::class,
        ];

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $choices = $choices + [
                "Administration : demande de validation d'un flux" => Type\FluxSubmitType::class,
                "Administration : annulation d'une demande de validation d'un flux" => Type\FluxUnsubmitType::class,
            ];
        }

        $builder
            ->add('excludedMailerNotificationTypes', ChoiceType::class, array(
                'label' => 'Notifications email',
                'choices' => $choices,
                'required' => false,
                'multiple' => true,
                'expanded' => true
            ))
        ;

        // prepare raw list to inverse in modelTransformer
        $values = array_values($choices);

        // add a model transform to inverse the selection (unselected => excluded)
        $builder->get('excludedMailerNotificationTypes')
            ->addModelTransformer(new CallbackTransformer(
                function ($input) use ($values)  {
                    return array_values(array_diff($values, $input));
                },
                function ($output) use ($values) {
                    return array_values(array_diff($values, $output));
                }
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
