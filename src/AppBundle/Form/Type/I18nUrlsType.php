<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class I18nUrlsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locales = ['en', 'de', 'nl', 'it', 'es', 'ru', 'zh'];
        foreach($locales as $locale) {
            $builder->add($locale, UrlType::class, [
                'label' => 'label.locale.' . $locale,
                //'label_attr' => array('title' => 'label.locale.' . $locale)
            ]);
        }
    }

}