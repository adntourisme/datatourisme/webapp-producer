<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Organization;
use Datatourisme\Bundle\WebAppBundle\Form\RoleType;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserType.
 */
class UserType extends AbstractType
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * TokenStorage.
     */
    protected $tokenStorage;

    public function __construct(Registry $registry, TokenStorage $tokenStorage)
    {
        $this->registry = $registry;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role', RoleType::class, array(
            'choice_label' => array(
                'ROLE_USER' => 'Producteur',
                'ROLE_ADMIN' => 'Administrateur',
                'ROLE_SUPER_ADMIN' => 'Super administrateur',
            ),
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->get('role')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onRolePostSubmit'));
    }

    /**
     * Add the organization field for new user OR ROLE_USER user.
     *
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        /** @var AdvancedUserInterface $user */
        $user = $event->getData();
        $form = $event->getForm();

        // determine if the user is new
        $new = ($user->getId() == null);

        if (!$new) {
            if ($this->tokenStorage->getToken()->getUser()->getId() === $user->getId() ||
                $this->tokenStorage->getToken()->getUser()->getRole() !== 'ROLE_SUPER_ADMIN' ||
                $user->getRole() === 'ROLE_USER') {
                $form->remove('role');
            } else {
                $roleConfig = $form->get('role')->getConfig();
                $options = $roleConfig->getOptions();
                foreach ($options['choices'] as $key => $choice) {
                    if (strstr($choice, 'ROLE_USER')) {
                        unset($options['choices'][$key]);
                    }
                }
                $form->add('role', ChoiceType::class, $options);
            }
        }

        // if the user is new or his current role is ROLE_USER
        if ($new || $user->getRole() == 'ROLE_USER') {
            // build choices
            $choices = $this->registry->getRepository(Organization::class)->findAll();

            // prepare options
            $options = array(
                'label' => 'Structure',
                'choices' => $choices,
                'choice_label' => function ($value) {
                    return $value->getName() ?? 'Nouvelle structure';
                },
                'choice_value' => function ($value) {
                    return $value !== null ? $value->getId() ?? 'new' : null;
                },
            );

            if ($new) {
                // add some options for new user
                $options['choices'] = array_merge(
                    $options['choices'],
                    array('-------------------' => array(new Organization()))
                );
                $options['help'] = "Si la structure de l'utilisateur n'existe pas dans DATAtourisme, sélectionnez <strong>Nouvelle structure</strong> pour compléter les informations de la nouvelle structure lors d'une seconde étape.";
                $options['required'] = false;
                $options['group_attr'] = array(
                    'data-visibility-target' => '#user_role',
                    'data-visibility-value' => 'ROLE_USER',
                    'class' => 'required',
                );
            }

            // add the organization field
            $form->add('organization', ChoiceType::class, $options);
        }
    }

    /**
     * Organization is required if role is not ROLE_USER.
     *
     * @param FormEvent $event
     */
    public function onRolePostSubmit(FormEvent $event)
    {
        $role = $event->getForm()->getData();
        $form = $event->getForm()->getParent();
        $organization = $form->get('organization')->getData();
        if ($role === 'ROLE_USER' && $organization == null) {
            $options = $form->get('organization')->getConfig()->getOptions();
            $form
                ->remove('organization')
                ->add('organization', ChoiceType::class, array_merge($options, array(
                    'constraints' => array(
                        new NotBlank(),
                    ),
                )));
        }
    }

    public function getParent()
    {
        return AccountType::class;
    }
}
