<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Faq;
use AppBundle\Entity\FaqTheme;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class FaqType.
 */
class FaqType extends AbstractType
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * FaqType constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = array_merge(
//            $this->registry->getRepository(FaqTheme::class)->findAll(),
            $this->registry->getRepository(FaqTheme::class)->findBy([], ['name' => 'ASC']),
            array('--------------------------------' => [new FaqTheme()])
        );

        $builder
            ->add('theme', ChoiceType::class, array(
                'label' => 'Thème',
                'choices' => $choices,
                'choice_label' => function ($theme) {
                    return $theme->getName() ?? 'Nouveau thème';
                },
                'choice_value' => function ($theme) {
                    return $theme !== null ? $theme->getName() ?? 'new' : null;
                },
            ))
            ->add('new_theme', TextType::class, array(
                'label' => 'Nouveau thème',
                'mapped' => false,
                'group_attr' => array(
                    'data-visibility-target' => '#faq_theme',
                    'data-visibility-value' => 'new',
                    'class' => 'required',
                ),
                'required' => false,
            ))
            ->add('question', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Question...',
                ),
                'required' => true,
                'label' => 'Question',
            ))
            ->add('answer', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Réponse...',
                    'rows' => '10',
                ),
                'required' => true,
                'label' => 'Réponse',
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Enregistrer',
                'attr' => array('class' => 'submit btn btn-primary'),
            ));

        $builder->get('theme')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onThemePostSubmit'));
        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => Faq::class,
            ));
    }

    /**
     * @param FormEvent $event
     */
    public function onThemePostSubmit(FormEvent $event)
    {
        $theme = $event->getForm()->getData();
        $form = $event->getForm()->getParent();
        if (!$theme->getId()) {
            $options = $form->get('new_theme')->getConfig()->getOptions();
            $form
                ->remove('new_theme')
                ->add('new_theme', TextType::class, array_merge($options, array(
                    'constraints' => array(
                        new NotBlank(),
                    ),
                )));
        }
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $faq = $event->getData();
        $form = $event->getForm();
        $newThemeName = $form->get('new_theme')->getData();
        if (!$faq->getTheme()->getId() && $newThemeName) {
            $faq->getTheme()->setName($newThemeName);
        }
        $event->setData($faq);
    }
}
