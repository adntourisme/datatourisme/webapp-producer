<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\ArrayToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $i = 0;
        foreach ($options['children'] as $class => $info) {
            $key = md5($class);
            $builder->add($key, TextareaType::class, array(
                'label' => $info['label'],
                'data' => isset($options['values'][$class]) ? $options['values'][$class]->getValues() : null,
                'required' => false,
                'attr' => array(
                    'shortIri' => $info['shortIri'],
                    'isClickable' => $info['isClickable'],
                    'rows' => 3,
                ),
            ));
            $builder->get($key)->addModelTransformer(new ArrayToStringTransformer("\r\n"));
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['breadcrumb'] = $options['breadcrumb'];
        $view->vars['type'] = $options['type'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('values', 'children'));
        $resolver->setDefault('breadcrumb', []);
        $resolver->setDefault('type', null);
    }
}
