<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Flux;
use AppBundle\Entity\ProcessStrategy;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FluxDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('processStrategy', EntityType::class, array(
                'label' => 'Stratégie',
                'class' => ProcessStrategy::class,
            ))
            ->add('mode', ChoiceType::class, array(
                'label' => 'Mode',
                'choices' => array(
                    Flux::$modeDefinitions[Flux::MODE_MANUAL][0] => Flux::MODE_MANUAL,
                    Flux::$modeDefinitions[Flux::MODE_PULL][0] => Flux::MODE_PULL,
                    Flux::$modeDefinitions[Flux::MODE_PUSH][0] => Flux::MODE_PUSH,
                ),
            ))
            ->add('fluxCrawler', FluxCrawlerType::class, array(
                'required' => false,
                'sshKey' => $options['sshKey'],
                'group_attr' => array(
                    'data-visibility-target' => '#flux_detail_mode',
                    'data-visibility-value' => Flux::MODE_PULL,
                    'class' => 'required',
                ),
            ))
            ->add('success_url', UrlType::class, array(
                'label' => 'Succès',
                'help' => 'L\'adresse spécifiée doit être accessible par la plateforme et fera l\'objet d\'une requête POST en cas de succès de l\'intégration du flux.',
                'attr' => array(
                    'placeHolder' => 'Adresse URL à notifier en cas de succès d\'intégration',
                ),
                'required' => false,
            ))
            ->add('fail_url', UrlType::class, array(
                'label' => 'Échec',
                'help' => 'L\'adresse spécifiée doit être accessible par la plateforme et fera l\'objet d\'une requête POST en cas d\'échec de l\'intégration du flux.',
                'attr' => array(
                    'placeHolder' => 'Adresse URL à notifier en cas d\'échec d\'intégration',
                ),
                'required' => false,
            ))
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        // notification url can not target this domain to protect api routes
        foreach (array('success_url', 'fail_url') as $field) {
            $url = $form->get($field)->getData();
            // host without port
            if (isset($_SERVER['SERVER_PORT']) && isset($_SERVER['HTTP_HOST'])) {
                $host = str_replace(':'.$_SERVER['SERVER_PORT'], '', $_SERVER['HTTP_HOST']);
                if ($url && parse_url($url, PHP_URL_HOST) === $host) {
                    $form->get($field)->addError(new FormError('L\'adresse ne peut pas pointer vers ce domaine'));
                }
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'sshKey' => null,
        ));
    }

    public function getParent()
    {
        return FluxType::class;
    }
}
