<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = '-- Tout --';

        $builder->add('fullName', TextFilterType::class, array(
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                // expressions that represent the condition
                $builder = $filterQuery->getExpressionBuilder();
                $expression = $builder->expr()->orX(
                    $builder->stringLike('u.lastName', $values['value']),
                    $builder->stringLike('u.firstName', $values['value'])
                );

                return $filterQuery->createCondition($expression);
            },
        ));
        $builder->add('organization', EntityFilterType::class, array(
            'class' => Organization::class,
            'placeholder' => $defaultPlaceholder,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('o')->orderBy('o.name', 'ASC');
            },
        ));

        $builder->add('role', ChoiceFilterType::class, array(
            'choices' => array(
                'Producteur' => 'ROLE_USER',
                'Administrateur' => 'ROLE_ADMIN',
                'Super administrateur' => 'ROLE_SUPER_ADMIN',
            ),
            'placeholder' => $defaultPlaceholder,
        ));

        $builder->add('email', TextFilterType::class);
        $builder->add('lastLogin', TextFilterType::class, array(
            'apply_filter' => false,
        ));

        $builder->add('status', ChoiceFilterType::class, array(
            'choices' => array(
                'Bloqué' => 'accountLocked',
                'En attente' => 'notEnabled',
                'Expiré' => 'accountExpired',
                'Mot de passe expiré' => 'credentialsExpired',
                'Temporairement bloqué' => 'temporallyBlocked',
                'Validé' => 'enabled',
            ),
            'placeholder' => $defaultPlaceholder,
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $builder = $filterQuery->getExpressionBuilder();
                switch ($values['value']) {
                    case 'accountLocked':
                        return $filterQuery->createCondition($builder->expr()->eq('u.accountNonLocked', 'false'));
                    case 'accountExpired':
                        return $filterQuery->createCondition($builder->expr()->eq('u.accountNonExpired', 'false'));
                    case 'notEnabled':
                        return $filterQuery->createCondition($builder->expr()->eq('u.enabled', 'false'));
                    case 'credentialsExpired':
                        return $filterQuery->createCondition($builder->expr()->eq('u.credentialsNonExpired', 'false'));
                    case 'temporallyBlocked':
                        return $filterQuery->createCondition($builder->dateInRange('u.failLoginDatetime', new \DateTime()));
                    case 'enabled':
                        return $filterQuery->createCondition($builder->expr()->eq('u.enabled', 'true'));
                    default:
                        return;
                }
            },
        ));
    }
}
