<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use AppBundle\Entity\OrganizationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrganizationFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextFilterType::class);
        $builder->add('type', EntityFilterType::class, array(
            'class' => OrganizationType::class,
            'placeholder' => '-- Tout --',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('t')->orderBy('t.name', 'ASC');
            },
        ));
        $builder->add('zip', TextFilterType::class);
        $builder->add('city', TextFilterType::class);
    }
}
