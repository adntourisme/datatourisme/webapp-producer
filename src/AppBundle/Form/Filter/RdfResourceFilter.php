<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\Flux;
use AppBundle\Entity\RdfResource;
use AppBundle\Thesaurus\HierarchyBuilder\HierarchyBuilder;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RdfResourceFilter.
 */
class RdfResourceFilter extends AbstractType
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var HierarchyBuilder
     */
    private $ontology;

    /**
     * @param Registry $registry
     */
    public function setRegistry(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param HierarchyBuilder $ontology
     */
    public function setOntology(HierarchyBuilder $ontology)
    {
        $this->ontology = $ontology;
        $this->ontology->loadOntology();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = '-- Tout --';

        $flux = $options['flux'];
        if (false === $flux instanceof Flux) {
            $errorValue = gettype($flux) !== 'object' ? gettype($flux) : get_class($flux);
            throw new \Exception('"flux" option\'s value must be instance of '.Flux::class.', '.$errorValue.' given.');
        }

        $builder
            // ____ IDENT
            ->add('identitySIT', TextFilterType::class)

            // ____ LABEL
            ->add('label', TextFilterType::class)

            // ____ TYPE
            ->add('type', ChoiceFilterType::class, array(
                'choices' => $types = (function (): array {
                    $poiIri = $this->ontology->extendIri(':PointOfInterest');
                    $children = $this->ontology->getChildren($poiIri, 1)[$poiIri];
                    $types = array();
                    foreach ($children['hierarchy'] as $iri => $values) {
                        $types[$values['label']] = $iri;
                    }
                    ksort($types);

                    return $types;
                })(),
                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values) use (&$types): bool {
                    if (empty($values['value'])) {
                        return false;
                    }
                    $qb = $filterQuery->getQueryBuilder();
                    $qb->andWhere('rdf_resource.type = :type');
                    $qb->setParameter('type', array_search($values['value'], $types));

                    return true;
                },
                'placeholder' => $defaultPlaceholder,
            ))

            // ____ CITY
            ->add('city', ChoiceFilterType::class, array(
                'choices' => (function () use ($flux): array {
                    $choices = $this->registry->getRepository(RdfResource::class)->findCityList($flux);
                    $choices = array_filter($choices);
                    sort($choices);

                    return $choices;
                })(),
                'choice_label' => function (string $city): string {
                    return $city;
                },
                'placeholder' => $defaultPlaceholder,
                'apply_filter' => true,
            ))

            // ____ STATUS
            ->add('status', ChoiceFilterType::class, array(
                'choices' => (function (): array {
                    $choices = array();
                    $statusList = RdfResource::statusList();
                    foreach ($statusList as $status) {
                        $choices[RdfResource::$statusDefinitions[$status][0]] = $status;
                    }
                    ksort($choices);

                    return $choices;
                })(),
                'placeholder' => $defaultPlaceholder,
            ))

            // ____ ANOMALY
            ->add('anomaly', TextFilterType::class, array(
                'apply_filter' => false,
            ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'flux' => new Flux(),
        ));
    }
}
