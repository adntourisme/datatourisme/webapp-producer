<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class ConceptionFluxFilter.
 */
class ConceptionFluxFilter extends AbstractType
{
    /**
     * @var AuthorizationChecker
     */
    private $checker;

    /**
     * SupportPostType constructor.
     *
     * @param AuthorizationChecker $checker
     */
    public function __construct(AuthorizationChecker $checker)
    {
        $this->checker = $checker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = '-- Tout --';

        $builder
            // ____ ORGANIZATION (Removed on PRE_SET_DATA if mandatory right is not granted)
            ->add('organization', EntityFilterType::class, array(
                'class' => Organization::class,
                'placeholder' => $defaultPlaceholder,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')->orderBy('o.name', 'ASC');
                },
            ))

            // ____ NAME
            ->add('name', TextFilterType::class)

            // ____ STATUS
            ->add('status', ChoiceFilterType::class, array(
                'choices' => (function (): array {
                    $choices = [];
                    $isAdmin = $this->checker->isGranted('ROLE_ADMIN');
                    foreach (Flux::productionStatusList() as $productionStatus) {
                        $choices[Flux::$statusDefinitions[$productionStatus][0]] = $productionStatus;
                    }
                    foreach (Flux::conceptionStatusList() as $conceptionStatus) {
                        if ($isAdmin && $conceptionStatus === Flux::STATUS_DRAFT) {
                            continue;
                        }
                        $choices[Flux::$statusDefinitions[$conceptionStatus][0]] = $conceptionStatus;
                    }
                    ksort($choices);

                    return $choices;
                })(),
                'placeholder' => $defaultPlaceholder,
            ))

            // ____ RULESET
            ->add('ruleset', ChoiceFilterType::class, array(
                'placeholder' => $defaultPlaceholder,
                'choices' => (function (): array {
                    $pendingStatus = AlignmentRulesetRevision::STATUS_PENDING;
                    $draftStatus = AlignmentRulesetRevision::STATUS_DRAFT;
                    $noneStatus = AlignmentRulesetRevision::STATUS_NONE;
                    $definitions = AlignmentRulesetRevision::$statusDefinitions;

                    $choices = [
                        $definitions[$noneStatus][0] => $noneStatus,
                        $definitions[$pendingStatus][0] => $pendingStatus,
                        $definitions[$draftStatus][0] => $draftStatus,
                    ];
                    ksort($choices);

                    return $choices;
                })(),
                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values): bool {
                    if (empty($values['value'])) {
                        return false;
                    }

                    $builder = $filterQuery->getQueryBuilder();
                    switch ($values['value']) {
                        case AlignmentRulesetRevision::STATUS_PENDING:
                            $builder->andWhere('draft_revision.status = :pending_status');
                            $builder->setParameter('pending_status', AlignmentRulesetRevision::STATUS_PENDING);
                            break;
                        case AlignmentRulesetRevision::STATUS_DRAFT:
                            $builder->andWhere('draft_revision.status = :draft_status');
                            $builder->setParameter('draft_status', AlignmentRulesetRevision::STATUS_DRAFT);
                            break;
                        case AlignmentRulesetRevision::STATUS_NONE:
                            $builder->andWhere('flux.alignmentRuleset IS NULL');
                            break;
                        default:
                            throw new \LogicException('This code should not be reached');
                    }

                    return true;
                },
            ))

            // ____ EVENTS
            ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))

        ;
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        if (true === $this->checker->isGranted('ROLE_ADMIN')) {
            $form->remove('ruleset');
        }
        if (false === $this->checker->isGranted('flux.filter.organization')) {
            $form->remove('organization');
        }
    }
}
