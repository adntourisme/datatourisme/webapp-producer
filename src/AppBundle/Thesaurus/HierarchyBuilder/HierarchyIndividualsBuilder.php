<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\HierarchyBuilder;

/**
 * Class HierarchyBuilder.
 */
class HierarchyIndividualsBuilder extends HierarchyBuilder
{
    /**
     * @param string $class
     *
     * @return bool
     */
    public function isOntologyClassExists($class)
    {
        $ignoredClasses = array(
            $this->defaultNamespaces['ontology'].'City',
            $this->defaultNamespaces['ontology'].'Department',
            $this->defaultNamespaces['ontology'].'Region',
            'http://schema.org/Country',
        );

        if (in_array($this->extendIri($class), $ignoredClasses)) {
            return true;
        }

        return $this->ontology->classExists($class);
    }

    /**
     * @param string $class
     *
     * @return string
     */
    public function getOntologyClassLabel($class)
    {
        $name = $this->ontology->getClassLabel($class);
        if ('http://schema.org/Country' === $name) {
            $name = 'Pays';
        }

        return $name;
    }

    /**
     * Return flat ascendant hierarchy for breadcrumb and navigation menu.
     *
     * @param $class
     *
     * @return array
     */
    public function getParents($class)
    {
        $parents = array();
        $continue = true;
        $this->extendIri($class);

        if ($class) {
            if ($class === $this->extendIri($this->getPrefixName('http://schema.org/').':Country')) {
                return array($class => 'Pays');
            }
            while ($continue) {
                $label = $this->getOntologyClassLabel($class);
                $parents[$class] = $label;
                $continue = false;
                foreach ($this->getOntologyValues() as $rdfClass => $info) {
                    $narrowers = $this->getValues($info, $this->subProperty);
                    if (!empty($narrowers)) {
                        if (in_array($class, $narrowers) && $this->isNamespaceAllowed($rdfClass)) {
                            $class = $rdfClass;
                            $continue = true;
                            break;
                        }
                    }
                }
            }
        }
        $parents = $this->extendIris($parents);

        return array_reverse($parents);
    }

    /**
     * @param $class
     *
     * @return array
     */
    protected function getOntologySubClasses($class)
    {
        if (strstr($class, $this->defaultNamespaces['thesaurus'])) {
            return parent::getOntologySubClasses($class);
        }

        $values = array();
        $class = $this->extendIri($class);
        foreach ($this->getOntologyValues() as $rdfClass => $value) {
            if (isset($value[$this->parentProperty]) && in_array($class, $value[$this->parentProperty])) {
                $values[] = $rdfClass;
            }
        }

        foreach ($values as $key => $value) {
            if (!$this->isNamespaceAllowed($value)) {
                unset($values[$key]);
            }
        }

        return $values;
    }
}
