<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\Cache;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;

class Cache
{
    /**
     * @var FilesystemAdapter
     */
    protected $cache;

    /**
     * @var CacheItem
     */
    protected $prefix;

    /**
     * Get cache file.
     *
     * @param $namespace
     * @param $environment
     * @param string|null $cacheDir
     *
     * @return CacheItem
     */
    public function get($namespace, $environment, $cacheDir)
    {
        if (!$cacheDir) {
            $cacheDir = '../var/cache/'.$environment;
        }

        if (!preg_match('#[^-+_.A-Za-z0-9]#', $namespace, $match)) {
            $cacheDir .= '/thesaurus/';
            $this->cache = new FilesystemAdapter($namespace, 0, $cacheDir);
            $this->prefix = $this->cache->getItem($namespace);

            return $this->prefix;
        }

        return null;
    }

    /**
     * Save cache file.
     *
     * @param $values
     *
     * @return bool
     */
    public function save($values)
    {
        if ($this->prefix) {
            $this->prefix->set($values);

            return $this->cache->save($this->prefix);
        }

        return false;
    }
}
