<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class NotificationRepository.
 */
class NotificationRepository extends EntityRepository
{
    public function findByUser(User $user, $limit = null)
    {
        $qb = $this->createQueryBuilder('n');

        $qb->andWhere('n.role = :role OR n.role IS NULL')
            ->setParameter('role', $user->getRole());

        if ($user->getRole() !== 'ROLE_ADMIN') {
            $qb->andWhere('n.organization = :organization OR n.organization IS NULL')
                ->setParameter('organization', $user->getOrganization());
        }

        // limit
        if ($limit) {
            $date = new \DateTime();
            $date->modify('-'.$limit);
            $qb->andWhere('n.dateTime > :date')
                ->setParameter(':date', $date);
        }

        // order
        $qb->addOrderBy('n.dateTime', 'DESC');

        return $qb->getQuery()
            ->getResult();
    }
}
