<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Process;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class ProcessRepository.
 */
class ProcessRepository extends EntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Flux      $flux
     * @param \DateTime $date
     *
     * @return Process|null
     */
    public function findLastProcessFromDay(Flux $flux, \DateTime $date, $status = Process::STATUS_SUCCESS)
    {
        $qb = $this->createQueryBuilder('process');

        // Flux parameter
        $qb->where('process.flux = :flux');
        $qb->setParameter('flux', $flux);

        // Date parameter
        $qb->andWhere('DATE(process.createdAt) <= DATE(:date)');
        $qb->setParameter('date', $date);

        // Status parameter
        if (is_string($status)) {
            $qb->andWhere('process.status = :status');
        } elseif (is_array($status)) {
            $qb->andWhere('process.status IN (:status)');
        } else {
            throw new \UnexpectedValueException('Expected string or array, '.gettype($status).' given.');
        }
        $qb->setParameter('status', $status);

        // Order by
        $qb->addorderBy('process.createdAt', 'DESC');
        $qb->addOrderBy('process.id', 'DESC');

        // Limit
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $fluxIds
     *
     * @return array
     */
    public function getPublishedPoiEvolution($fluxIds)
    {
        $pastDayRange = 10;
        $now = new \DateTime();
        $i = 0;
        $processesByDate = array();
        while ($i++ < $pastDayRange) {
            $now->modify('-1 days');
            $qb = $this->createQueryBuilder('process');
            $qb->select('flux.id as flux_id, process.nbPublishedRdfResources')
                ->leftJoin('process.flux', 'flux', Join::WITH, 'process.flux = flux')
                ->where('DATE(process.createdAt) = DATE(:date)')
                ->andWhere('flux.id in (:ids)')
                ->andWhere('process.status = :status')
                ->orderBy('process.createdAt', 'DESC')
                ->setParameter('status', Process::STATUS_SUCCESS)
                ->setParameter('date', $now)
                ->setParameter('ids', $fluxIds);
            $result = $qb->getQuery()->execute();

            $fluxIdPassed = array();
            $nbrItems = 0;
            foreach ($result as $res) {
                if (!isset($fluxIdPassed[$res['flux_id']])) {
                    $nbrItems += $res['nbPublishedRdfResources'];
                    $fluxIdPassed[$res['flux_id']] = $res['flux_id'];
                }
            }

            if ($nbrItems > 0) {
                $processesByDate[$now->format('Y-m-d')] = $nbrItems;
            }
        }

        return $processesByDate;
    }

    /**
     * @param Organization|array $organizations
     * @param array              $criteria
     * @param array              $orderBy
     * @param array              $groupBy
     * @param int|null           $limit
     *
     * @return array
     *
     * @throws \Exception
     */
    public function findByOrganizations($organizations = null, array $criteria = [], array $orderBy = [], array $groupBy = [], int $limit = null): array
    {
        $qb = $this->createQueryBuilder('process');

        // ____ Limiting to given organization
        if ($organizations instanceof Organization) {
            $qb->join('process.flux', 'flux', Join::WITH, 'flux.organization = :o');
            $qb->where('flux.organization = :o');
            $qb->setParameter('o', $organizations);
        } elseif (is_array($organizations)) {
            $qb->join('process.flux', 'flux');
            $count = count($organizations);
            for ($i = 0; $i < $count; ++$i) {
                if (null === $organizations[$i]) {
                    continue;
                } elseif (false === $organizations[$i] instanceof Organization) {
                    $errorValue = gettype($organizations[$i]) !== 'object' ? gettype($organizations[$i]) : get_class($organizations[$i]);
                    throw new \Exception('Organization must be instance of '.Organization::class.' or null, '.$errorValue.' given.');
                }

                $qb->orWhere("flux.organization = :o_$i");
                $qb->setParameter("o_$i", $organizations[$i]);
            }
        }

        // ____ Criteria
        $i = 0;
        foreach ($criteria as $key => $value) {
            $qb->andWhere("process.$key = :criteria_value_$i");
            $qb->setParameter("criteria_value_$i", $value);
            ++$i;
        }

        // ____ Order by
        foreach ($orderBy as $key => $value) {
            if (!is_string($value) || (strtolower($value) !== 'asc' && strtolower($value) !== 'desc')) {
                throw new \Exception('Order By values can only be "asc" or "desc", '.$value.' given.');
            }
            $qb->addOrderBy("process.$key", $value);
        }

        // ____ Group by
        $count = count($groupBy);
        for ($i = 0; $i < $count; ++$i) {
            $qb->addGroupBy("process.$groupBy[$i]");
        }

        // ____ Limit
        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Flux|null $flux
     *
     * @return QueryBuilder
     */
    public function createPaginationQueryBuilder(Flux $flux = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('process');

        $qb->addSelect('
            CASE WHEN process.statsProduceRDF IS NULL THEN 0
            ELSE CAST(GET_JSON_SUB_VALUE(process.statsProduceRDF, \'anomalies\', \'total\') AS integer)
            END
            AS anomaly_count
        ');

        $qb->leftJoin('process.files', 'files');
        $qb->addSelect('COUNT (DISTINCT files) AS file_count');
        $qb->addSelect('SUM (DISTINCT files.size) AS total_size');
        $qb->addSelect('process.evolution AS published_rate');

        $qb->leftjoin('process.flux', 'flux');
        if ($flux) {
            $qb->andWhere('process.flux = :f');
            $qb->setParameter('f', $flux);
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->leftJoin('flux.organization', 'organization');
            $qb->addGroupBy('organization.id');
        } else {
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }

        $qb->addGroupBy('process.id');
        $qb->addGroupBy('flux.id');

        $qb->addOrderBy('process.createdAt');
        $qb->addOrderBy('process.id');

        return $qb;
    }

    /**
     * @param Flux      $flux
     * @param \DateTime $furthestDate
     * @param array     $statusList
     * @param int       $x
     *
     * @return array
     */
    public function findByMaxDatePlusX(\DateTime $furthestDate, Flux $flux = null,
                                       array $statusList = [], int $x = 0
    ): array {
        $qb = $this->createQueryBuilder('process');

        $qb->where('process.createdAt >= :furthest_date');
        $qb->setParameter('furthest_date', $furthestDate);

        $qb->andWhere('process.status IN (:status_list)');
        $qb->setParameter('status_list', $statusList);

        if ($flux !== null) {
            $qb->andWhere('process.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        if ($x > 0) {
            $qb->orWhere(
                $qb->expr()->in(
                    'process.id', (function () use (&$flux, &$x): string {
                        $qb2 = $this->createQueryBuilder('p');
                        $qb2->where('p.createdAt < :furthest_date');
                        if ($flux !== null) {
                            $qb2->andWhere('process.flux = :flux');
                        }
                        $qb2->andWhere('process.status IN (:status_list)');
                        $qb2->orderBy('p.createdAt', 'DESC');
                        $qb2->setMaxResults($x);

                        return $qb2->getDQL();
                    })()
                )
            );
        }

        $qb->orderBy('process.createdAt, process.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function getCount(Flux $flux = null): int
    {
        $qb = $this->createQueryBuilder('process');

        $qb->select('COUNT(DISTINCT process.id)');

        if ($flux) {
            $qb->where('process.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->innerJoin('process.flux', 'flux');
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
