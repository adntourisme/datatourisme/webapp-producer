<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

abstract class AbstractStatsEntryRepository extends EntityRepository
{
    const SIGN_GT = '>';
    const SIGN_LT = '<';
    const SIGN_GTE = '>=';
    const SIGN_LTE = '<=';

    /** @var TokenStorageInterface $tokenStorage */
    protected $tokenStorage;

    /** @var AuthorizationChecker $authorizationChecker */
    protected $authorizationChecker;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Finds entries for current day.
     *
     * @param Flux|null $flux
     * @param bool      $getQb
     *
     * @return array|QueryBuilder
     */
    public function findForToday(Flux $flux = null, bool $getQb = false)
    {
        $date = (new \DateTime())->setTimestamp(strtotime('today'));

        return $this->findByDay($date, $flux, $getQb);
    }

    /**
     * Finds entries by day extracted from given date.
     *
     * @param \DateTime $date
     * @param Flux      $flux
     * @param bool      $getQb
     *
     * @return array|QueryBuilder
     */
    public function findByDay(\DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        $qb = $this->createQueryBuilder('entry');

        $qb->where('entry.date = :date');
        $qb->setParameter('date', $date);

        if ($flux) {
            $qb->andWhere('entry.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        $qb->addOrderBy('entry.date', 'DESC');
        $qb->addOrderBy('entry.id', 'DESC');

        $this->restrictQueryBuilder($qb);

        return $getQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * Finds entries between two given dates.
     *
     * @param \DateTime $furthestDate
     * @param \DateTime $nearestDate
     * @param bool      $includeDates if true, includes given dates in the interval
     * @param Flux|null $flux
     * @param bool      $getQb
     *
     * @return array|QueryBuilder
     */
    public function findBetweenDates(\DateTime $furthestDate, \DateTime $nearestDate,
                                     bool $includeDates = true, Flux $flux = null, bool $getQb = false
    ) {
        if (true === $includeDates) {
            $furthestSign = self::SIGN_GTE;
            $nearestSign = self::SIGN_LTE;
        } else {
            $furthestSign = self::SIGN_GT;
            $nearestSign = self::SIGN_LT;
        }

        $qb = $this->createQueryBuilder('entry');

        $qb->where("entry.date $furthestSign :furthest_date");
        $qb->setParameter('furthest_date', $furthestDate);

        $qb->andWhere("entry.date $nearestSign :nearest_date");
        $qb->setParameter('nearest_date', $nearestDate);

        if ($flux) {
            $qb->andWhere('entry.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        $qb->addOrderBy('entry.date', 'DESC');
        $qb->addOrderBy('entry.id', 'DESC');

        $this->restrictQueryBuilder($qb);

        return $getQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * @param int       $maxDays
     * @param Flux|null $flux
     * @param bool      $getQb
     *
     * @return int|QueryBuilder
     */
    public function findNbPublishedForLastDays(int $maxDays, Flux $flux = null, bool $getQb = false)
    {
        $furthestDate = (new \DateTime())->setTimestamp(
            time() - ((time() - strtotime('today')) + 86400 * $maxDays)
        );

        $qb = $this->createQueryBuilder('entry');

        $qb->select('SUM(entry.nbPublishedRdfResources)');

        $qb->where('entry.date >= :furthest_date');
        $qb->setParameter('date', $furthestDate);

        if ($flux) {
            $qb->select('entry.nbPublishedRdfResources');
            $qb->andWhere('entry.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        $qb->addOrderBy('entry.date', 'DESC');
        $qb->addOrderBy('entry.id', 'DESC');

        $this->restrictQueryBuilder($qb);

        return $getQb ? $qb : $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Finds entries from current day to given days interval.
     *
     * @param int       $maxDays the number of days to fetch backward from today
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    public function findForLastDays(int $maxDays, Flux $flux = null, bool $getQb = false)
    {
        $furthestDate = (new \DateTime())->setTimestamp(
            time() - ((time() - strtotime('today')) + 86400 * $maxDays)
        );

        return $this->findAfterOrOnDate($furthestDate, $flux, $getQb);
    }

    /**
     * Finds entries after given date.
     *
     * @param \DateTime $date
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    public function findAfterDate(\DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        return $this->findAroundDate(self::SIGN_GT, $date, $flux, $getQb);
    }

    /**
     * Finds entries after or on given date.
     *
     * @param \DateTime $date
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    public function findAfterOrOnDate(\DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        return $this->findAroundDate(self::SIGN_GTE, $date, $flux, $getQb);
    }

    /**
     * Finds entries before given date.
     *
     * @param \DateTime $date
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    public function findBeforeDate(\DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        return $this->findAroundDate(self::SIGN_LT, $date, $flux, $getQb);
    }

    /**
     * Finds entries after or on given date.
     *
     * @param \DateTime $date
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    public function findBeforeOrOnDate(\DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        return $this->findAroundDate(self::SIGN_LTE, $date, $flux, $getQb);
    }

    /**
     * Finds entries after or before given date, according to given comparison sign.
     *
     * @param string    $sign
     * @param \DateTime $date
     * @param Flux|null $flux
     *
     * @return array|QueryBuilder
     */
    protected function findAroundDate(string $sign, \DateTime $date, Flux $flux = null, bool $getQb = false)
    {
        if (!in_array($sign, [self::SIGN_GT, self::SIGN_GTE, self::SIGN_LT, self::SIGN_LTE], true)) {
            throw new \UnexpectedValueException('Unrecognized comparison sign.');
        }

        $qb = $this->createQueryBuilder('entry');

        $qb->where("entry.date $sign :date");
        $qb->setParameter('date', $date);

        if ($flux) {
            $qb->andWhere('entry.flux = :flux');
            $qb->setParameter('flux', $flux);
        }

        $qb->addOrderBy('entry.date', 'DESC');
        $qb->addOrderBy('entry.id', 'DESC');

        $this->restrictQueryBuilder($qb);

        return $getQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $qb
     */
    protected function restrictQueryBuilder(QueryBuilder &$qb)
    {
        if ($token = $this->tokenStorage->getToken() && !$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->andWhere('entry.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }
    }

    /**
     * @param Organization $organization
     */
    protected function checkOrganizationAccess(Organization $organization)
    {
        if (false === $this->isOrganizationAuthorized($organization)) {
            throw new AccessDeniedException("User cannot access to given organization's statistics.");
        }
    }

    /**
     * @param Organization $organization
     *
     * @return bool
     */
    protected function isOrganizationAuthorized(Organization $organization)
    {
        if ($token = $this->tokenStorage->getToken()) {
            if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                if ($organization !== $token->getUser()->getOrganization()) {
                    return false;
                }
            }
        }

        return true;
    }
}
