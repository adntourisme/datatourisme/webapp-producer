<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\AlignmentRulesetRevision;
use Doctrine\ORM\EntityRepository;

/**
 * Class AlignmentRulesetRevisionRepository.
 */
class AlignmentRulesetRevisionRepository extends EntityRepository
{
    public function findByAndWithout(array $criteria, array $excludedRevisions = array(), array $order = array(), array $groupBy = array(), int $limit = null): array
    {
        $qb = $this->createQueryBuilder('revision');

        // ____ Criteria
        $i = 0;
        foreach ($criteria as $key => $value) {
            $qb->andWhere("revision.$key = :criteria_value_$i");
            $qb->setParameter("criteria_value_$i", $value);
            ++$i;
        }

        // ____ Excluding
        $count = count($excludedRevisions);
        $i = 0;
        while ($i < $count) {
            if ($excludedRevisions[$i] === null) {
                ++$i;
                continue;
            } elseif (false === $excludedRevisions[$i] instanceof AlignmentRulesetRevision) {
                $errorValue = gettype($excludedRevisions[$i]) !== 'object' ? gettype($excludedRevisions[$i]) : get_class($excludedRevisions[$i]);
                throw new \Exception('Excluded revision must be instance of '.AlignmentRulesetRevision::class.' or null, '.$errorValue.' given.');
            }

            $qb->andWhere($qb->expr()->not("revision.id = :excluded_id_$i"));
            $qb->setParameter("excluded_id_$i", $excludedRevisions[$i]->getId());

            ++$i;
        }

        // ____ Order by
        foreach ($order as $key => $value) {
            if (!is_string($value) || (strtolower($value) !== 'asc' && strtolower($value) !== 'desc')) {
                throw new \Exception('Order By values can only be "asc" or "desc", '.$value.' given.');
            }
            $qb->addOrderBy("revision.$key", $value);
        }

        // ____ Group by
        $count = count($groupBy);
        for ($i = 0; $i < $count; ++$i) {
            $qb->addGroupBy("revision.$groupBy[$i]");
        }

        // ____ Limit
        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        $result = $qb->getQuery()->getResult();

        return $result;
    }
}
