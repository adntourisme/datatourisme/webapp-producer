<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Faq;
use AppBundle\Entity\FaqTheme;
use Doctrine\ORM\EntityRepository;

/**
 * Class FaqThemeRepository.
 */
class FaqThemeRepository extends EntityRepository
{
    /**
     * Reorganizes theme's faqs' positions to avoid any holes or other unexpected states. Useful on actions such as deletes, edits, etc.
     *
     * @param FaqTheme $theme
     *
     * @return bool
     */
    public function sortTheme(FaqTheme $theme): bool
    {
        $em = $this->getEntityManager();

        /** @var Faq[] $faqs */
        $faqs = $theme->getFaqs();
        $sortedFaqs = array();

        $count = count($faqs);
        $newPos = $count;
        foreach ($faqs as $faq) {
            if ($faq->getPosition() === null) {
                $faq->setPosition($newPos++);
            }
            $sortedFaqs[] = $faq;
        }

        usort($sortedFaqs, function (Faq $a, Faq $b) {
            return $a->getPosition() <=> $b->getPosition();
        });

        $count = count($sortedFaqs);
        for ($i = 0; $i < $count; ++$i) {
            $sortedFaqs[$i]->setPosition($i + 1);
        }

        $em->flush();

        return true;
    }
}
