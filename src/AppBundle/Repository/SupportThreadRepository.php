<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\SupportPost;
use AppBundle\Entity\SupportThread;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class SupportThreadRepository.
 */
class SupportThreadRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param bool $restricted
     * @param int  $limit
     * @param int  $offset
     *
     * @return array
     *
     * @throws \Exception
     */
    public function findByLastPostsDates(User $user, bool $restricted = true, int $limit = 0, int $offset = 0): array
    {
        $query = $this->getQueryLastPostDates($user, $restricted, $limit, $offset);
        $threads = $query->getResult();

        return $threads;
    }

    /**
     * If $count === true, performs a COUNT query and returns int. Else, returns array of results.
     *
     * @param User     $user
     * @param bool     $restricted
     * @param string   $order
     * @param int|null $limit
     *
     * @return array
     *
     * @throws \Exception
     */
    public function findUnreadThreads(User $user, bool $restricted = true, string $order = 'ASC', int $limit = null): array
    {
        if ($order !== 'ASC' && $order !== 'DESC') {
            throw new \Exception('"order" parameter in '.__METHOD__.' can only take "ASC" or "DESC", "'.$order.'" given.');
        } elseif (is_int($limit) && $limit < 0) {
            throw new \Exception('"limit" parameter in '.__METHOD__.' can only take null or positive integer, "'.$limit.'" given".');
        }

        $sql = '
            SELECT DISTINCT ON (id) * FROM
            (
                SELECT thread.*, post.created_at
                FROM support_thread AS thread, support_post AS post
                WHERE thread.id = post.thread_id
            ';

        $sql .= $restricted === true
                ? ' AND thread.organization_id = :o'
                : ''
            ;

        $sql .= '
                AND NOT EXISTS (
                    SELECT read.*
                    FROM support_thread_read AS read
                    WHERE thread.id = read.thread_id
                    AND read.user_id = :u
                    AND post.created_at <= read.date
                )
                GROUP BY thread.id, post.created_at
                ORDER BY post.created_at '.$order.'
            )
            AS t'
        ;

        $sql .= is_int($limit)
            ? ' LIMIT '.$limit
            : ''
        ;

        $em = $this->getEntityManager();
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(SupportThread::class, 't');
        $query = $em->createNativeQuery($sql, $rsm);

        $query->setParameter('u', $user->getId());
        if ($restricted === true) {
            $query->setParameter('o', $user->getOrganization()->getId());
        }

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param User $user
     * @param bool $restricted
     *
     * @return int
     */
    public function countUnreadThreads(User $user, bool $restricted = true): int
    {
        $sql = '
            SELECT COUNT(*)
            FROM support_thread AS thread, support_post AS post
            WHERE thread.id = post.thread_id
        ';

        $sql .= $restricted === true
            ? ' AND thread.organization_id = :o'
            : ''
        ;

        $sql .= '
            AND NOT EXISTS (
                SELECT read.*
                FROM support_thread_read AS read
                WHERE thread.id = read.thread_id
                AND read.user_id = :u
                AND post.created_at <= read.date
            )
            GROUP BY thread.id
        ';

        $em = $this->getEntityManager();
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(SupportThread::class, 't');
        $query = $em->createNativeQuery($sql, $rsm);

        $query->setParameter('u', $user->getId());
        if ($restricted === true) {
            $query->setParameter('o', $user->getOrganization()->getId());
        }

        $result = count($query->getScalarResult());

        return $result;
    }

    /**
     * @param User $user
     * @param bool $restricted
     * @param int  $limit
     * @param int  $offset
     *
     * @return Query
     *
     * @throws \Exception
     */
    public function getQueryLastPostDates(User $user, bool $restricted = true, int $limit = 0, int $offset = 0): Query
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('t, MAX (p.createdAt) AS HIDDEN last_date')
            ->from(SupportThread::class, 't')
            ->leftJoin(SupportPost::class, 'p', Join::WITH, 'p.thread = t');

        if ($restricted === true) {
            $qb
                ->andWhere('t.organization = :organization')
                ->setParameter('organization', $user->getOrganization());
        }

        $qb
            ->groupBy('t')
            ->orderBy('last_date', 'DESC')
            ->setFirstResult($offset);

        if ($limit > 0) {
            $qb
                ->setMaxResults($limit);
        }

        $query = $qb->getQuery();

        return $query;
    }
}
