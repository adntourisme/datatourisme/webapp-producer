<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\AlignmentRuleset;
use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\JavaWorker\JavaWorkerClient;
use GuzzleHttp\Exception\RequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Process\Exception\RuntimeException;

/**
 * Class AlignmentRulesetController.
 *
 * @Route("/flux/{id}/ruleset", requirements={"id": "\d+"})
 * @Security("is_granted('flux.edit', flux)")
 */
class AlignmentRulesetController extends Controller
{
    /**
     * @Route("/create", name="flux.ruleset.create")
     */
    public function createAction(Flux $flux, Request $request)
    {
        if ($flux->getAlignmentRuleset() != null) {
            return $this->redirectToRoute('flux.ruleset.index', ['id' => $flux->getId()]);
            //throw new BadRequestHttpException("Il existe déjà une règle d'alignement liée à ce flux");
        }

        // prepare flatten POI type hierarchy
        $uri = $this->getParameter('namespaces')['ontology'].'PointOfInterest';

        // prepare type thesaurus choices list
        $this->get('app.thesaurus.hierarchy.classes_builder')->loadOntology();
        $hierarchyOntology = $this->get('app.thesaurus.hierarchy.classes_builder')->getChildren($uri, -1);

        $flatten = function ($uri, $item, $depth = 0, $parent = null) use (&$flatten) {
            $values = array();
            if ($parent) {
                $values = [array(
                    'uri' => $uri,
                    'label' => $item['label'],
                    'depth' => $depth,
                    'parent' => $parent,
                )];
                ++$depth;
            }
            uasort($item['hierarchy'], function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
            foreach ($item['hierarchy'] as $uri2 => $item) {
                $values = array_merge($values, $flatten($uri2, $item, $depth, $uri));
            }

            return $values;
        };
        $typeChoices = $flatten($uri, current($hierarchyOntology), 0);

        // actual thesaurus alignments
        $iris = array_map(function ($item) { return $item['uri']; }, $typeChoices);

        // prepare correspondence values
        $thesaurusValues = array();
        $alignmentValues = $this->get('app.thesaurus_storage')->getValues($flux->getOrganization(), $iris);
        foreach ($alignmentValues as $uri => $values) {
            foreach ($values as $value) {
                $thesaurusValues[$value] = $uri;
            }
        }

        if ($request->getMethod() == 'POST') {
            $params = json_decode($request->getContent(), true);

            // prepare thesaurus in right format
            $thesaurus = array();
            if (isset($params['thesaurus'])) {
                foreach ($params['thesaurus'] as $value => $uri) {
                    $thesaurus[$uri] = isset($thesaurus[$uri]) ? $thesaurus[$uri] : array();
                    $thesaurus[$uri][] = $value;
                }
            }

            try {
                // run next task
                $javaWorkerClient = $this->get('app.java_worker.client');
                $payload = array(
                    'reset' => true,
                    'alignment' => (object) $params['alignment'],
                    'thesaurus' => (object) $thesaurus,
                );
                $javaWorkerClient->runTask($flux, 'process_xml', null, $payload);

                // save thesaurus
                if (count($thesaurus)) {
                    $this->get('app.thesaurus_storage')->addValues($flux->getOrganization(), $thesaurus);
                }

                // save alignment ruleset
                $alignmentRulesetRevision = new AlignmentRulesetRevision();
                $this->get('state_machine.alignment_ruleset_revision_operation')->getMarking($alignmentRulesetRevision);
                $alignmentRulesetRevision->setContent($params['alignment']);
                $alignmentRulesetRevision->setVersion(1);
                $alignmentRuleset = new AlignmentRuleset($flux);
                $alignmentRuleset->addRevision($alignmentRulesetRevision);
                $alignmentRulesetRevision->setAuthor($this->getUser());
                $alignmentRulesetRevision->setUpdater($this->getUser());
                $flux->setAlignmentRuleset($alignmentRuleset);
                $em = $this->getDoctrine()->getManager();

                $em->persist($alignmentRuleset);
                $em->flush();

                return new JsonResponse([
                    'success' => true,
                    'redirect' => $this->generateUrl('flux.ruleset.revision.editor', array(
                        'id' => $flux->getId(),
                        'revision_id' => $alignmentRulesetRevision->getId(),
                    )),
                ], 200);
            } catch (RequestException $e) {
                return JavaWorkerClient::handleRequestException($e);
            } catch (RuntimeException $e) {
                return JsonResponse::create(['error' => ['message' => $e->getMessage(), 'code' => 500]], 500);
            }
        }

        return $this->render('flux/ruleset/create.html.twig', array(
            'flux' => $flux,
            'typeChoices' => $typeChoices,
            'thesaurusValues' => $thesaurusValues,
        ));
    }

    /**
     * @Route(
     *     "/process-file",
     *     name="flux.ruleset.process_file"
     * )
     * @Method({"POST"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function processFileAction(Flux $flux, Request $request)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        $file->getExtension();
        $file->getRealPath();

        $final = $file->getRealPath().'.'.$file->getClientOriginalExtension();
        $file->move(dirname($final), basename($final));

        $payload = array('source' => 'file://'.$final);
        $javaWorkerClient = $this->get('app.java_worker.client');

        try {
            $javaWorkerClient->runTask($flux, 'download', 'preprocess_xml', $payload);

            return $this->redirectToRoute('api.xpath_schema', ['flux_id' => $flux->getId()]);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }
}
