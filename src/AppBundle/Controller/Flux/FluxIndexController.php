<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Form\Filter\ConceptionFluxFilter;
use AppBundle\Form\Filter\ProductionFluxFilter;
use AppBundle\Repository\FluxRepository;
use AppBundle\Services\ProcessEvolution;
use Datatourisme\Bundle\WebAppBundle\Utils\CsvWriter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/flux")
 */
class FluxIndexController extends Controller
{
    /**
     * @Route("/production", name="flux.production")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function productionAction(Request $request): Response
    {
        $pageSize = 15; // Rows per page
        $paginationOptions = array(
            'distinct' => true,
            'wrap-queries' => true,
            'defaultSortFieldName' => 'last_process_date',
            'defaultSortDirection' => 'desc',
        );

        $qb = $this->get('app.repository.flux')->createProductionQueryBuilder();

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, ProductionFluxFilter::class)
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        $items = $list->getItems();

        return $this->render('flux/production.html.twig', array(
            'list' => $list,
            'items' => $items,
            'page_size' => $pageSize,
            'revision_status_definitions' => AlignmentRulesetRevision::$statusDefinitions,
        ));
    }

    /**
     * @Route ("/conception", name="flux.conception")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function conceptionAction(Request $request): Response
    {
        $pageSize = 15; // Rows per page

        $paginationOptions = array(
            'distinct' => true,
            'wrap-queries' => true,
            'defaultSortFieldName' => 'flux.updatedAt',
            'defaultSortDirection' => 'desc',
        );

        $qb = $this->get('app.repository.flux')->createConceptionQueryBuilder();
        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, ConceptionFluxFilter::class)
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);
        $items = $list->getItems();

        return $this->render('flux/conception.html.twig', array(
            'list' => $list,
            'items' => $items,
            'page_size' => $pageSize,
            'revision_status_definitions' => AlignmentRulesetRevision::$statusDefinitions,
        ));
    }

    /**
     * @Route("/sparkline/{id}/{maxDays}",
     *        name="flux.sparkline",
     *        requirements={"id" = "\d+",
     *                      "maxDays" = "\d+"},
     *        defaults={"maxDays": 10})
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sparklineAction(Flux $flux, int $maxDays = 10): Response
    {
        $data = $this->get('app.utils.process_evolution')->getData(
            ProcessEvolution::SPARKLINE, $flux, $maxDays, 'Y/m/d'
        );

        return $this->render('flux/sparkline.html.twig', array(
         'published_graph' => $data['published_graph'],
         'published_sum' => $data['published_sum'],
        ));
    }

    /**
     * @Route("/production/export", name="flux.production.export")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function productionExportAction(Request $request): Response
    {
        $writer = new CsvWriter([
            'Flux' => '[0].name',
            'Structure' => '[0].organization.name',
            'Statut' => function($row) {
                return $row['status'] ? Flux::$statusDefinitions[$row['status']][0] : null;
            },
            'Dernier traitement' => '[last_process_date]',
            'POI' => '[nb_rdf_resources]'
        ]);

        /** @var FluxRepository $userRepository */
        $qb = $this->get('app.repository.flux')->createProductionQueryBuilder();

        // apply filter
        $form = $this->createForm(ProductionFluxFilter::class);
        $form->submit($request->query->get($form->getName()));
        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $qb);

        $rows = $qb->getQuery()->execute();
        $writer->writeObjects($rows);

        $writer->output("flux-production.csv");
        exit;
    }
}
