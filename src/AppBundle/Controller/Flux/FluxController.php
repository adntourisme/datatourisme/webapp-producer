<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux;
use AppBundle\Entity\ProcessStrategy;
use AppBundle\Form\Type\FluxType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class FluxController.
 *
 * @Route("/flux")
 */
class FluxController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/create", name="flux.create")
     * @Security("is_granted('flux.create')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $flux = new Flux();
        $flux->setOrganization($this->getUser()->getOrganization());

        $form = $this->createForm(FluxType::class, $flux);

        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $processStrategy = $em->getRepository('AppBundle:ProcessStrategy')->find(ProcessStrategy::COMPLETE);
            $flux->setProcessStrategy($processStrategy);
            $em->persist($flux);
            $em->flush();
            $this->addFlash('success', 'Le flux a bien été créé.');

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('flux.ruleset.index', array('id' => $flux->getId())),
            ], 200);
        }

        return $this->render('flux/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/administrate/delete", name="flux.administrate.delete")
     * @Security("is_granted('flux.delete', flux)")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Flux $flux)
    {
        if ($flux->isActive()) {
            throw new BadRequestHttpException('This flux is not deletable.');
        }

        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() === 'POST') {
            if ($flux->getStatus() == Flux::STATUS_DRAFT) {
                $em->remove($flux);
                $em->flush();
                $this->addFlash('warning', 'Le flux a été supprimé.');

                return new JsonResponse([
                    'success' => true,
                    'redirect' => $this->generateUrl('flux.production'),
                ], 200);
            } else {
                $this->get('app.task_runner')->delete($flux);
                $this->addFlash('warning', 'La suppression du flux a été programmée. Il sera définitivement supprimé lorsque le dataset aura été nettoyé des données concernées.');

                return new JsonResponse([
                    'success' => true,
                    'reload' => true,
                ], 200);
            }
        }

        return $this->render('admin/flux/administrate/confirmation.html.twig', [
            'flux' => $flux,
            'level' => 'danger',
            'action' => 'supprimer le flux',
            'form' => false,
        ]);
    }

    /**
     * @Route("/{id}/administrate/pause", name="flux.administrate.pause")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pauseAction(Request $request, Flux $flux)
    {
        return $this->administrateAction($request, $flux, [
            'name' => 'pause',
            'level-notify' => 'warning',
            'action' => 'Mettre en veille le flux',
            'result' => 'Le flux a été mis en veille.',
        ]);
    }

    /**
     * @Route("/{id}/administrate/unpause", name="flux.administrate.unpause")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unpauseAction(Request $request, Flux $flux)
    {
        return $this->administrateAction($request, $flux, [
            'name' => 'unpause',
            'level-notify' => 'success',
            'action' => 'Sortir de veille le flux',
            'result' => 'Le flux a été sorti de sa veille.',
        ]);
    }

    /**
     * @Route("/{id}/administrate/block", name="flux.administrate.block")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blockAction(Request $request, Flux $flux)
    {
        return $this->administrateAction($request, $flux, [
            'name' => 'block',
            'level' => 'warning',
            'action' => 'Bloquer le flux',
            'result' => 'Le flux a été bloqué.',
        ]);
    }

    /**
     * @Route("/{id}/administrate/unblock", name="flux.administrate.unblock")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unblockAction(Request $request, Flux $flux)
    {
        return $this->administrateAction($request, $flux, [
            'name' => 'unblock',
            'level' => 'warning',
            'level-notify' => 'success',
            'action' => 'Débloquer le flux',
            'result' => 'Le flux a été débloqué.',
        ]);
    }

    /**
     * @param Request  $request
     * @param Flux     $flux
     * @param string[] $action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrateAction(Request $request, Flux $flux, array $action): Response
    {
        $workflow = $this->get('state_machine.flux_operation');

        if ($request->getMethod() === 'POST') {
            $workflow->apply($flux, $action['name']);
            // Flush for workflow
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(($action['level-notify'] ?? ($action['level'] ?? '')), $action['result']);

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('admin/flux/administrate/confirmation.html.twig', [
            'flux' => $flux,
            'level' => $action['level'] ?? false,
            'action' => $action['action'],
            'form' => false,
        ]);
    }
}
