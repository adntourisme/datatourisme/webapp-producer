<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\AlignmentRuleset;
use AppBundle\Entity\Anomaly;
use AppBundle\Entity\Flux;
use AppBundle\Entity\ProcessStrategy;
use AppBundle\Notification\Type\FluxSubmitType;
use AppBundle\Notification\Type\FluxUnsubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\AlignmentRulesetRevision;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class FluxRuleSetRevisionController.
 *
 * @Route("/flux/{id}/ruleset/revision", requirements={"id": "\d+"})
 */
class AlignmentRulesetRevisionController extends Controller
{
    /**
     * @Security("is_granted('flux.see', flux)")
     *
     * @Route("", name="flux.ruleset.index")
     *
     * @param Flux $flux
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function indexAction(Flux $flux)
    {
        $d = $this->getDoctrine();
        $revisionRepository = $d->getRepository(AlignmentRulesetRevision::class);
        $alignmentRuleset = $flux->getAlignmentRuleset();

        if ($alignmentRuleset) {
            $revisionVersion = $alignmentRuleset->getActiveVersion() ?? $alignmentRuleset->getDraftVersion();

            if ($revisionVersion) {
                $revision = $revisionRepository->findOneBy(array(
                    'version' => $revisionVersion,
                    'alignmentRuleset' => $alignmentRuleset,
                ));
                if (!$revision) {
                    throw new \Exception('Active or Draft version should exist');
                }
            }
        }

        return $this->render('flux/ruleset/revision/index.html.twig', array(
            'flux' => $flux,
            'revisions' => $alignmentRuleset ? $revisionRepository->findBy(['alignmentRuleset' => $alignmentRuleset], ['version' => 'desc']) : null,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $d->getRepository(Anomaly::class)->getCountByFlux($flux),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * @Security("is_granted('alignment_ruleset_revision.create', flux)")
     *
     * @Route("/create", name="flux.ruleset.revision.create")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function createAction(Flux $flux)
    {
        $alignmentRuleset = $flux->getAlignmentRuleset();

        if (!$alignmentRuleset) {
            $alignmentRuleset = new AlignmentRuleset($flux);
            $flux->setAlignmentRuleset($alignmentRuleset);
        }

        $revision = new AlignmentRulesetRevision();
        $this->get('state_machine.alignment_ruleset_revision_operation')->getMarking($revision);
        $revision->setAuthor($this->getUser());
        $revision->setUpdater($this->getUser());
        $maxRevisionVersion = 0;
        if ($flux->getAlignmentRuleset() && $flux->getAlignmentRuleset()->getActive() !== null) {
            /* @var AlignmentRulesetRevision $revision */
            foreach ($flux->getAlignmentRuleset()->getRevisions() as $existingRevision) {
                if ($existingRevision->getVersion() > $maxRevisionVersion) {
                    $maxRevisionVersion = $existingRevision->getVersion();
                }
            }
            $lastRevisionContent = $flux->getAlignmentRuleset()->getActive()->getContent();
            if ($lastRevisionContent) {
                $revision->setContent($lastRevisionContent);
            }
        }
        $revision->setVersion($maxRevisionVersion + 1);
        $revision->setAlignmentRuleset($alignmentRuleset);
        $alignmentRuleset->addRevision($revision);

        $em = $this->getDoctrine()->getManager();
        if (!$flux->getAlignmentRuleset()->getId()) {
            $em->persist($flux->getAlignmentRuleset());
        }
        $em->persist($revision);
        $flux->getAlignmentRuleset()->setDraftVersion($revision->getVersion());
        $em->flush();
        $this->addFlash('success', 'La révision a été créée');

        return $this->redirectToRoute('flux.ruleset.index', array('id' => $flux->getId(), 'revision_id' => $revision->getId()));
    }

    /**
     * @Security("is_granted('alignment_ruleset_revision.validate')")
     *
     * @Route("/{revision_id}/validate", name="flux.ruleset.revision.validate")
     *
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Request                  $request
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     *
     * @return Response
     */
    public function validateAction(Request $request, Flux $flux, AlignmentRulesetRevision $revision)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() !== $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be activated');
        }

        if (!$revision->hasRules()) {
            throw new AccessDeniedException('Revision without rules cannot be activated');
        }

        if ($request->getMethod() === 'POST') {
            $this->get('state_machine.alignment_ruleset_revision_operation')
                 ->apply($revision, 'validate');

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La révision a bien été validée, un traitement du flux a été programmé.');

            // schedule process from process_xml task
            $this->get('app.task_runner')->start($flux, null, 'process_xml');

            return $this->redirectToRoute('flux.ruleset.index', array('id' => $flux->getId(), 'revision_id' => $revision->getId()));
        }

        return $this->render('flux/ruleset/revision/activate.html.twig', array(
           'revision' => $revision,
        ));
    }

    /**
     * @Security("is_granted('flux.preview', flux)")
     *
     * @Route("/{revision_id}/comment", name="flux.ruleset.revision.comment")
     *
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @return Response
     */
    public function commentAction(Flux $flux, AlignmentRulesetRevision $revision, Request $request)
    {
        $form = null;
        if ($this->isGranted('alignment_ruleset_revision.edit', $revision)) {

            $form = $this->createFormBuilder($revision)
                ->add('comment', TextareaType::class, array(
                    'attr' => array(
                        'rows' => '8',
                    ),
                    'required' => false,
                    'label' => 'Commentaire',
                ))
                ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Le commentaire de la révision a bien été enregistré.');
                return new JsonResponse(['success' => true, 'reload' => true], 200);
            }  

        } else {
            if (empty($revision->getComment())) {
                throw $this->createNotFoundException();
            }
        }
    
        return $this->render('flux/ruleset/revision/comment.html.twig', array(
            'flux' => $flux,
            'revision' => $revision,
            'form' => $form ? $form->createView() : null,
        ));
    }

    /**
     * @Security("is_granted('alignment_ruleset_revision.submit', revision)")
     *
     * @Route("/{revision_id}/submit", name="flux.ruleset.revision.submit")
     *
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Request                  $request
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     *
     * @return Response
     */
    public function submitAction(Request $request, Flux $flux, AlignmentRulesetRevision $revision)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() !== $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be submitted');
        }

        if (!$revision->hasRules()) {
            throw new AccessDeniedException('Revision without rules cannot be submitted');
        }
        if ($request->getMethod() === 'POST') {
            $this->get('state_machine.alignment_ruleset_revision_operation')
                 ->apply($revision, 'submit');
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La demande d\'activation pour cette révision a été envoyée');
            $this->get('notifier')->dispatch(FluxSubmitType::class, $flux);

            return $this->redirectToRoute('flux.ruleset.index', array('id' => $flux->getId(), 'revision_id' => $revision->getId()));
        }

        return $this->render('flux/ruleset/revision/submit.html.twig', array(
            'revision' => $revision,
        ));
    }

    /**
     * @Security("is_granted('alignment_ruleset_revision.submit', revision)")
     *
     * @Route("/{revision_id}/unsubmit", name="flux.ruleset.revision.unsubmit")
     *
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Request                  $request
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     *
     * @return Response
     */
    public function unsubmitAction(Request $request, Flux $flux, AlignmentRulesetRevision $revision)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() !== $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be unsubmitted');
        }

        if ($request->getMethod() === 'POST') {
            $this->get('state_machine.alignment_ruleset_revision_operation')
                ->apply($revision, 'unsubmit');
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La demande d\'activation pour cette révision a été annulée');
            $this->get('notifier')->dispatch(FluxUnsubmitType::class, $flux);

            return $this->redirectToRoute('flux.ruleset.index', array('id' => $flux->getId(), 'revision_id' => $revision->getId()));
        }

        return $this->render('flux/ruleset/revision/unsubmit.html.twig', array(
            'revision' => $revision,
        ));
    }

    /**
     * @Security("is_granted('alignment_ruleset_revision.delete', revision)")
     * @Route("/{revision_id}/remove", name="flux.ruleset.revision.remove")
     *
     * @param Request                  $request
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     *
     * @return Response
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     */
    public function removeAction(Request $request, Flux $flux, AlignmentRulesetRevision $revision)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() != $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be removed');
        }

        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();
            $alignmentRuleset = $revision->getAlignmentRuleset();

            if ($alignmentRuleset->getRevisions()->count() < 2) {
                $em->remove($alignmentRuleset);
                $flux->setAlignmentRuleset(null);
            } elseif ($alignmentRuleset->getDraftVersion() == $revision->getVersion()) {
                $alignmentRuleset->removeRevision($revision);
                $alignmentRuleset->setDraftVersion(null);
            }
            $em->remove($revision);
            $em->flush();
            $this->addFlash('warning', 'La révision a été supprimée');

            return $this->redirectToRoute('flux.ruleset.index', array('id' => $flux->getId()));
        }

        return $this->render('flux/ruleset/revision/remove.html.twig', array(
            'revision' => $revision,
        ));
    }

    /**
     * @Security("is_granted('flux.edit', flux)")
     *
     * @Route("/{revision_id}/editor", name="flux.ruleset.revision.editor")
     *
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     *
     * @return Response
     */
    public function editorAction(Flux $flux, AlignmentRulesetRevision $revision)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() != $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be edited');
        }

        return $this->render('flux/ruleset/revision/editor.html.twig', array(
            'flux' => $flux,
            'revision' => $revision,
        ));
    }

    /**
     * @Security("is_granted('flux.preview', flux)")
     *
     * @Route("/{revision_id}/preview", name="flux.ruleset.revision.preview")
     *
     * @param Flux                     $flux
     * @param AlignmentRuleSetRevision $revision
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @return Response
     */
    public function previewAction(Flux $flux, AlignmentRulesetRevision $revision)
    {
        return $this->render('flux/ruleset/revision/preview.html.twig', array(
            'flux' => $flux,
            'revision' => $revision,
        ));
    }

    /**
     * SPECIAL ROUTE FOR DEBUG.
     *
     * @param AlignmentRuleSetRevision $revision
     *
     * @Route("/{revision_id}/raw", name="flux.ruleset.revision.raw")
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @return Response
     */
    public function rawAction(Flux $flux, AlignmentRulesetRevision $revision, Request $request)
    {
        if ($flux->getAlignmentRuleset()->getDraftVersion() != $revision->getVersion()) {
            throw new AccessDeniedException('Only draft revision can be edited');
        }

        $data = array(
            'content' => json_encode($revision->getContent(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
        );

        $form = $this->createFormBuilder($data)
            ->add('content', TextareaType::class, array('label' => false))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $content = json_decode($data['content'], JSON_OBJECT_AS_ARRAY);
            if ($content) {
                $revision->setContent($content);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'L\'alignement a été correctement modifié !');

                // if the flux is not in production, run process_xml task now
                if ($flux->getActiveAlignmentRevision() == null) {
                    $javaWorkerClient = $this->get('app.java_worker.client');
                    $payload = array(
                        'reset' => ($flux->getProcessStrategy()->getId() == ProcessStrategy::COMPLETE),
                        'alignment' => (object) $content,
                        'thesaurus' => $this->get('app.thesaurus_storage')->getValues($flux->getOrganization()),
                    );
                    $javaWorkerClient->runTask($flux, 'process_xml', null, $payload);
                    $this->addFlash('success', 'Les structures XPath ont été ré-évaluées !');
                }
            } else {
                $form->get('content')->addError(new FormError('JSON invalide'));
                $this->addFlash('error', 'Le contenu JSON n\'est pas valide !');
            }
        }

        return $this->render('flux/ruleset/revision/raw.html.twig', array(
            'flux' => $flux,
            'revision' => $revision,
            'form' => $form->createView(),
        ));
    }
}
