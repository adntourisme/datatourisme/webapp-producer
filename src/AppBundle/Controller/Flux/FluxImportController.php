<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints as Constraint;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class FluxController.
 *
 * @Route("/flux/{id}/import")
 * @Security("is_granted('flux.start', flux)")
 */
class FluxImportController extends Controller
{
    public static $authorizedMimeTypes = array(
        'application/xml', 'application/zip', 'text/xml', 'text/plain',
    );

    /**
     * Pull file and start process.
     *
     * @Route("", name="flux.import", requirements={"id" = "[^/]+"}, defaults={ "_format": "json" })
     * @ParamConverter("flux", class="AppBundle:Flux", options={"mapping": {"id" : "apiId"}})
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @Method("POST")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importAction(Request $request, Flux $flux)
    {
        $code = 200;

        if ($flux->getMode() === Flux::MODE_PUSH) {
            if (!$this->isGranted('flux.start', $flux)) {
                $code = 403;
                $data = array('error' => "Ce flux n'est pas en production, il ne peut donc pas être programmé.");
            } else {
                $filename = $this->storeFileContent($request->getContent());
                if ($filename) {
                    $this->get('app.task_runner')->start($flux, $filename);
                    $data = array('success' => true);
                } else {
                    $code = 500;
                    $data = array('error' => ['message' => 'Seuls les fichiers XML et ZIP sont autorisés.', 'code' => $code]);
                }
            }
        } else {
            $code = 400;
            $data = array('error' => ['message' => "Le flux n'est pas configuré pour ce mode de mise à jour.", 'code' => $code]);
        }

        return new JsonResponse($data, $code);
    }

    /**
     * Upload flux file and start process.
     *
     * @Route("/manual", name="flux.import.manual")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \HttpInvalidParamException
     */
    public function importManualAction(Request $request, Flux $flux)
    {
        if ($flux->getMode() !== Flux::MODE_MANUAL) {
            throw new BadRequestHttpException('The flux is not configured for this update mode.');
        }

        $maxFilesize = ini_get('upload_max_filesize');
        $form = $this->createFormBuilder()
            ->add('file', FileType::class, array(
                'label' => 'Fichier',
                'multiple' => false,
                'required' => true,
                'help' => sprintf('Vous pouvez envoyer un fichier <strong>XML</strong> ou un fichier <strong>ZIP</strong> contenant exclusivement des fichier XML. La taille du fichier envoyé ne doit pas dépasser <strong>%s</strong>.', $maxFilesize),
                'constraints' => new Constraint\File(array(
                    'mimeTypes' => self::$authorizedMimeTypes,
                    'mimeTypesMessage' => 'Le format sélectionné n\'est pas supporté',
                    'maxSize' => $this->toBytes($maxFilesize),
                )),
            ))->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('file')->getData();
            $filename = $this->storeFileContent(file_get_contents($uploadedFile), $uploadedFile->getClientOriginalName());

            if (!$filename) {
                throw new \HttpInvalidParamException("Le format du fichier n'est pas reconnu");
            }

            switch ($flux->getStatus()) {
                // DRAFT MODE
                case Flux::STATUS_DRAFT:
                    $javaWorkerClient = $this->get('app.java_worker.client');
                    $payload = array(
                        'reset' => true,
                        'source' => $filename,
                        'alignment' => $flux->getAlignmentRuleset()->getDraft()->getContent(),
                        'thesaurus' => $this->get('app.thesaurus_storage')->getValues($flux->getOrganization()),
                    );
                    $javaWorkerClient->runTask($flux, 'download', 'process_xml', $payload);

                    $this->addFlash('success', 'Le fichier a été correctement mis à jour.');

                    return new JsonResponse(['success' => true, 'reload' => true], 200);
                    break;

                // PRODUCTION MODE
                case Flux::STATUS_PRODUCTION:
                    $this->get('app.task_runner')->start($flux, $filename);
                    $this->addFlash('success', 'Le traitement du flux a été programmé.');

                    return new JsonResponse([
                        'success' => true,
                        'redirect' => $this->generateUrl('flux.detail', array('id' => $flux->getId())),
                    ], 200);
                    break;
            }
        }

        return $this->render(':flux/import:manual.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Pull file and start process.
     *
     * @Route("/pull", name="flux.import.pull")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importPullAction(Request $request, Flux $flux)
    {
        if ($flux->getMode() !== Flux::MODE_PULL) {
            throw new BadRequestHttpException('The flux is not configured for this update mode.');
        }

        if ($request->getMethod() === 'POST') {
            $this->get('app.task_runner')->start($flux);
            $this->addFlash('success', 'Le traitement a correctement été programmé.');

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('flux.detail', array('id' => $flux->getId())),
            ], 200);
        }

        return $this->render(':flux/import:pull.html.twig', array(
            'flux' => $flux,
        ));
    }

    /**
     * Pull file and start process.
     *
     * @Route("/push", name="flux.import.push")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importPushAction(Request $request, Flux $flux)
    {
        if ($flux->getMode() !== Flux::MODE_PUSH) {
            throw new BadRequestHttpException('The flux is not configured for this update mode.');
        }

        return $this->render(':flux/import:push.html.twig', array(
            'url' => $this->generateUrl('flux.import', array('id' => $flux->getApiId()), UrlGeneratorInterface::ABSOLUTE_URL),
        ));
    }

    /**
     * Store content in tmp file and return uri.
     *
     * @param Flux $flux
     * @param $content
     *
     * @return null|string
     */
    protected function storeFileContent($content, $filename = null)
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'datatourisme-import-');
        file_put_contents($tmpFile, $content);
        $file = new File($tmpFile);

        // guess extension and store file in the correct repository
        if (in_array($file->getMimeType(), self::$authorizedMimeTypes)) {
            $extension = $file->guessExtension() == 'zip' ? 'zip' : 'xml';
            $tmpFile = $file->getPath().DIRECTORY_SEPARATOR.$file->getBasename().'.'.$extension;
            rename($file, $tmpFile);

            return 'file://'.realpath($tmpFile);
        }

        return null;
    }

    /**
     * Converts given value to bytes integer.
     *
     * If the input is a string with numeric value and size unit as
     * last character, it will be converted in bytes integer.
     * Example: Input "2M" would return "2097152".
     *
     * If the input is a numeric string or an integer, it will be
     * returned as an integer without any transformation.
     * Example: Input "2097152" would still return "2097152".
     *
     * @param string|int $val
     *
     * @return int
     */
    private function toBytes($val): int
    {
        if (is_int($val)) {
            return $val;
        } elseif (is_string($val)) {
            $str = trim($val);
            if (is_numeric($str)) {
                return (int) $str;
            }

            $strLen = strlen($str);
            $value = (int) substr($str, 0, $strLen - 1);
            $unit = strtolower($str[$strLen - 1]);
            switch ($unit) {
                case 'k':
                    return $value * 1024;
                case 'm':
                    return $value * 1024 ** 2;
                case 'g':
                    return $value * 1024 ** 3;
            }

            throw new \LogicException(
                'This code should not be reached. Size unit might not be valid (K, M or G expected).'
            );
        }

        throw new \UnexpectedValueException(
            'String or integer expected, '.(gettype($val) !== 'object' ? gettype($val) : get_class($val)).' given.'
        );
    }
}
