<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Organization;
use AppBundle\Form\Filter\OrganizationFilter;
use AppBundle\Form\Type\OrganizationType;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/organization")
 * @Security("has_role('ROLE_ADMIN')")
 */
class OrganizationController extends Controller
{
    /**
     * @Route("", name="organization.index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository(Organization::class)->createQueryBuilder('o');
        $query->leftJoin('AppBundle:OrganizationType', 't', Join::WITH, 't = o.type');

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($query, OrganizationFilter::class)
            ->handleRequest($request);

        return $this->render('admin/organization/index.html.twig', array(
            'list' => $list,
        ));
    }

    /**
     * @Route("/add", name="organization.add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $organization = new Organization();
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($organization);
            $em->flush();
            $this->addFlash('success', 'La structure a bien été créée');

            return $this->redirectToRoute('organization.edit', array('id' => $organization->getId()));
        }

        return $this->render('admin/organization/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="organization.edit")
     *
     * @param Request      $request
     * @param Organization $organization
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Organization $organization)
    {
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($organization);
            if ($form->get('file')->get('removeFile')->getData()) {
                $organization->removeUpload();
            }
            $em->flush();

            $this->addFlash('success', 'La structure a bien été mise à jour');

            return $this->redirectToRoute('organization.edit', array('id' => $organization->getId()));
        }

        return $this->render('admin/organization/edit.html.twig', [
            'form' => $form->createView(),
            'organization' => $organization,
        ]);
    }

    /**
     * @Route("/{id}/administrate/delete", name="organization.administrate.delete")
     * @Security("is_granted('organization.delete', organization)")
     *
     * @param Request      $request
     * @param Organization $organization
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Organization $organization)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() === 'POST') {
            $em->remove($organization);
            $em->flush();
            $this->addFlash('success', 'La structure a bien été supprimée.');
            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('organization.index')
            ], 200);
        }

        return $this->render('admin/organization/administrate/delete.html.twig', [
            'organization' => $organization
        ]);
    }

    /**
     * @Route("/{id}/administrate", name="organization.administrate")
     *
     * @param Organization|null $organization
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrateAction(Organization $organization)
    {
        return $this->render(':admin/organization:administrate.html.twig', [
            'organization' => $organization,
        ]);
    }
}
