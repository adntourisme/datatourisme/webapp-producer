<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Flux;
use AppBundle\Entity\LoginHistory;
use AppBundle\Entity\News;
use AppBundle\Entity\Process;
use AppBundle\Entity\Reuse;
use AppBundle\Entity\SupportPost;
use AppBundle\Entity\SupportThread;
use AppBundle\Entity\SupportThreadRead;
use AppBundle\Entity\User;
use AppBundle\Services\ProcessEvolution;
use AppBundle\Services\StatsEntryManager;
use Doctrine\ORM\Query\Expr\Join;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    protected $limit = 5;

    /**
     * @Route("", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('dashboard/index.html.twig');
    }

    /**
     * @Route("/dashboard/widget/kpis", name="dashboard.widget.kpis")
     *
     * @return Response
     */
    public function widgetKpisAction(): Response
    {
        $fluxStatsRepository = $this->get('app.repository.flux_stats_entry');
        $rdfResourceRepository = $this->get('app.repository.rdf_resource');
        $reuseRepository = $this->get('app.repository.reuse');

        $productionCount = $fluxStatsRepository->getCountByFluxStatus(Flux::STATUS_PRODUCTION);
        $publishedCount = $rdfResourceRepository->getPublishedCount();
        $totalCount = $rdfResourceRepository->getTotalCount();
        $publicationRatio = StatsEntryManager::calculatePublicationRatio($publishedCount, $totalCount, 2);
        $resourcesByPoi = $reuseRepository->getResourcesByPoi();

        return $this->render('dashboard/widget/kpis.html.twig', array(
            'flux' => $productionCount,
            'published_poi' => $publishedCount,
            'published_poi_ratio' => $publicationRatio,
            'reused_poi' => $resourcesByPoi['total'],
        ));
    }

    /**
     * @Route("/dashboard/widget/processes", name="dashboard.widget.processes")
     *
     * @return Response
     */
    public function widgetProcessesAction(): Response
    {
        $processRepository = $this->get('doctrine.orm.entity_manager')->getRepository(Process::class);
        $organization = $this->isGranted('ROLE_ADMIN') ? null : $this->getUser()->getOrganization();
        $processes = $processRepository->findByOrganizations($organization, [], ['createdAt' => 'desc', 'id' => 'desc'], [], $this->limit);

        return $this->render('dashboard/widget/processes.html.twig', array(
            'processes' => $processes,
        ));
    }

    /**
     * @Route("/dashboard/widget/production", name="dashboard.widget.production")
     *
     * @return Response
     */
    public function widgetProductionAction(): Response
    {
        $qb = $this->get('doctrine.orm.entity_manager')->getRepository(Flux::class)->createQueryBuilder('f');
        $qb
            ->join('f.processes', 'p', Join::WITH, 'p.flux = f')->addGroupBy('f')
            ->where('f.status in (:statusList)')
            ->setParameter('statusList', Flux::productionStatusList())
            ->orderBy('f.status', 'ASC')
            ->addOrderBy('f.name', 'ASC');

        if (!$this->isGranted('ROLE_ADMIN')) {
            $qb
                ->andWhere('f.organization = :organization')
                ->setParameter('organization', $this->getUser()->getOrganization());
        }

        return $this->render('dashboard/widget/production.html.twig', array(
            'flows' => $qb->getQuery()->getResult(),
        ));
    }

    /**
     * @Route("/dashboard/widget/validation", name="dashboard.widget.validation")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function widgetValidationAction(): Response
    {
        $qb = $this->get('doctrine.orm.entity_manager')->getRepository(Flux::class)->createQueryBuilder('f');
        $qb
            ->join('f.processes', 'p', Join::WITH, 'p.flux = f')->addGroupBy('f')
            ->where('f.status in (:statusList)')
            ->setParameter('statusList', Flux::conceptionStatusList())
            ->orderBy('f.updatedAt', 'ASC');
        $flows = $qb->getQuery()->getResult();

        return $this->render('dashboard/widget/validation.html.twig', array(
            'flows' => $flows,
        ));
    }

    /**
     * @Route("/dashboard/widget/login", name="dashboard.widget.login")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function widgetLoginAction(): Response
    {
        $qb = $this->get('doctrine.orm.entity_manager')->getRepository(LoginHistory::class)->createQueryBuilder('lh');
        $qb->join('lh.user', 'u', Join::WITH, 'lh.user = u')->groupBy('lh');
        $qb->join('u.organization', 'o', Join::WITH, 'u.organization = o')->groupBy('lh');
        $qb->orderBy('lh.datetime', 'DESC');
        $loginHistory = $qb->getQuery()->setMaxResults($this->limit)->getResult();

        return $this->render('dashboard/widget/login.html.twig', array(
            'loginHistory' => $loginHistory,
        ));
    }

    /**
     * @Route("/dashboard/widget/stats", name="dashboard.widget.stats")
     *
     * @return Response
     */
    public function widgetStatsAction(): Response
    {
        $rdfResourceRepository = $this->get('app.repository.rdf_resource');
        $publishedCount = $rdfResourceRepository->getPublishedCount();
        return $this->render('dashboard/widget/stats.html.twig', array(
            'evolution' => $this->get('app.utils.process_evolution')->getData(
                ProcessEvolution::MORRIS, null, 10, 'Y-m-d'
            ),
            'resources_by_poi' => $rdfResourceRepository->getPublishedByPoi(),
            'published_poi' => $publishedCount
        ));
    }

    /**
     * @Route("/dashboard/widget/support", name="dashboard.widget.support")
     *
     * @return Response
     */
    public function widgetSupportAction(): Response
    {
        $user = $this->getUser();
        $restricted = $this->isGranted('support.see.all_threads') === false;

        $em = $this->get('doctrine.orm.entity_manager');
        $threadReadRepository = $em->getRepository(SupportThreadRead::class);
        $threadRepository = $em->getRepository(SupportThread::class);
        $postRepository = $em->getRepository(SupportPost::class);

        $threads = $threadRepository->findByLastPostsDates($user, $restricted, 5);
        $messages = array();
        $count = count($threads);
        for ($i = 0; $i < $count; ++$i) {
            $messages[$i]['thread'] = $threads[$i];
            $messages[$i]['last_read'] = $threadReadRepository->findOneBy(['user' => $user, 'thread' => $threads[$i]]);

            $twoLastPosts = $postRepository->findBy(['thread' => $threads[$i]], ['createdAt' => 'DESC'], 2);
            $messages[$i]['is_new_thread'] = count($twoLastPosts) < 2;
            $messages[$i]['last_post'] = $twoLastPosts[0];
        }

        return $this->render('dashboard/widget/support.html.twig', array(
            'widget_messages' => $messages,
        ));
    }

    /**
     * @Route("/dashboard/widget/news", name="dashboard.widget.news")
     *
     * @return Response
     */
    public function widgetNewsAction(): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $newsRepository = $em->getRepository(News::class);
        $news = $newsRepository->findOneBy([], ['date' => 'DESC', 'id' => 'DESC']);
        return $this->render('dashboard/widget/news.html.twig', array(
            'news' => $news,
        ));
    }
}
