<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Datatourisme\Bundle\WebAppBundle\Entity\UploadableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="organization")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class Organization
{
    use ORMBehaviors\Timestampable\Timestampable;
    use UploadableTrait;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $name;

    /**
     * @var OrganizationType
     * @ORM\ManyToOne(targetEntity="OrganizationType", inversedBy="organizations")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="User", mappedBy="organization")
     */
    protected $users;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Flux", mappedBy="organization")
     */
    private $flux;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank()
     * @Assert\Length(max=5)
     */
    protected $zip;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     * @Assert\Length(max=20)
     */
    protected $phoneNumber;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $website;

    /**
     * @var bool
     * @ORM\Column(name="cgu", type="boolean")
     */
    protected $cgu;

    /**
     * Remove file. Used for model validation. Must not be persisted.
     *
     * @var bool
     */
    protected $removeFile;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->flux = new ArrayCollection();
        $this->cgu = false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return OrganizationType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param OrganizationType $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return ArrayCollection
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param ArrayCollection $flux
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return bool
     */
    public function isCgu()
    {
        return $this->cgu;
    }

    /**
     * @param bool $cgu
     */
    public function setCgu($cgu)
    {
        $this->cgu = $cgu;
    }

    /**
     * @return bool
     */
    public function isRemoveFile()
    {
        return $this->removeFile;
    }

    /**
     * @param bool $removeFile
     */
    public function setRemoveFile($removeFile)
    {
        $this->removeFile = $removeFile;
    }

    /**
     * @return int file max size
     */
    public function getMaxFileSize()
    {
        return 5242880;
    }

    /**
     * @return array
     */
    public function getAllowedMimeTypes()
    {
        return array(
            'image/jpeg',
            'image/png',
        );
    }

    /**
     * @return array
     */
    public function getNotAllowedMimeTypeMessage()
    {
        return 'Vous devez fournir une image uniquement.';
    }

    /**
     * @return string
     */
    protected function getTemplatesRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../web/uploads/organization';
    }
}
