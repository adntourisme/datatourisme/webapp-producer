<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Infos\Mode;

trait ModeInfosTrait
{
    /**
     * @var ModeInfos
     */
    private $modeInfos;

    /**
     * @param ModeInfos $modeInfos
     */
    public function setModeInfos(ModeInfos $modeInfos)
    {
        $this->modeInfos = $modeInfos;
    }

    /**
     * @param string $mode
     * @param string $label
     * @param string $description
     *
     * @return ModeInfos
     */
    public static function createModeInfos(string $mode, string $label, string $description): ModeInfos
    {
        $modeInfos = new ModeInfos();
        $modeInfos
            ->setMode($mode)
            ->setLabel($label)
            ->setDescription($description);

        return $modeInfos;
    }

    /**
     * Based on self mode definitions, returns a ModeInfos instance with following properties:.
     *
     * - "mode":        Machine code.
     * - "label":       Human readable title.
     * - "description": Human readable description.
     *
     * @return ModeInfos
     */
    abstract public function getModeInfos(): ModeInfos;
}
