<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Infos\Mode\ModeInfos;
use AppBundle\Entity\Infos\Mode\ModeInfosTrait;
use AppBundle\Entity\Infos\Status\StatusInfos;
use AppBundle\Entity\Infos\Status\StatusInfosTrait;
use AppBundle\Entity\Status\StatusableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="process")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcessRepository")
 */
class Process
{
    use Timestampable, StatusableTrait, StatusInfosTrait, ModeInfosTrait;

    const STATUS_WAIT = 'wait';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public static $statusDefinitions = array(
        self::STATUS_WAIT => [
            'En attente',
            'Le traitement est en attente.',
        ],
        self::STATUS_RUNNING => [
            'En cours',
            'Le traitement est en cours.',
        ],
        self::STATUS_SUCCESS => [
            'Succès',
            'Le traitement s\'est correctement déroulé.',
        ],
        self::STATUS_ERROR => [
            'Erreur',
            'Le traitement a rencontré une erreur bloquante empêchant son déroulemenht.',
        ],
    );

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Assert\Length(max=63)
     */
    private $uuid;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=63)
     */
    private $mode;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Assert\Length(max=63)
     */
    private $progress;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastJob;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
     * @Assert\NotBlank()
     */
    private $nbPublishedRdfResources = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
     * @Assert\NotBlank()
     */
    private $nbExcludedRdfResources = 0;

    /**
     * @var string
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $statsProcessXML;

    /**
     * @var string
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $statsProduceRDF;

    /**
     * @var string
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $statsPersistRDF;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
     * @Assert\NotBlank()
     */
    private $duration = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     * @Assert\NotBlank()
     */
    private $evolution = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=false)
     * @Assert\NotBlank()
     */
    private $status;

    /**
     * @var Flux
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="processes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $flux;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Log", mappedBy="process")
     */
    private $logs;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Anomaly", mappedBy="process")
     */
    private $anomalies;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="File", mappedBy="process")
     */
    private $files;

    /**
     * Unmapped. Logic purposes.
     *
     * @var Log
     */
    private $reasonError;

    /**
     * Process constructor.
     *
     * @param Flux|null $flux
     */
    public function __construct($flux = null)
    {
        $this->logs = new ArrayCollection();
        $this->anomalies = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->flux = $flux;
        $this->setMode(Flux::MODE_MANUAL);
        $this->setStatus(self::STATUS_WAIT);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     *
     * @return Process
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbPublishedRdfResources(): int
    {
        return $this->nbPublishedRdfResources;
    }

    /**
     * @param int $nbPublishedRdfResources
     */
    public function setNbPublishedRdfResources(int $nbPublishedRdfResources)
    {
        $this->nbPublishedRdfResources = $nbPublishedRdfResources;
    }

    /**
     * @return int
     */
    public function getNbRdfResources()
    {
        return $this->nbPublishedRdfResources + $this->nbExcludedRdfResources;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return float
     */
    public function getEvolution()
    {
        return $this->evolution;
    }

    /**
     * @param float $evolution
     */
    public function setEvolution(float $evolution)
    {
        $this->evolution = $evolution;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Process
     */
    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     *
     * @return Process
     */
    public function setFlux(Flux $flux)
    {
        $this->flux = $flux;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param ArrayCollection $logs
     */
    public function setLogs(ArrayCollection $logs)
    {
        $this->logs = $logs;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnomalies()
    {
        return $this->anomalies;
    }

    /**
     * @param ArrayCollection $anomalies
     */
    public function setAnomalies(ArrayCollection $anomalies)
    {
        $this->anomalies = $anomalies;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     */
    public function setFiles(ArrayCollection $files)
    {
        $this->files = $files;
    }

    /**
     * @param File $file
     *
     * @return bool
     */
    public function addFile(File $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param string $progress
     *
     * @return Process
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    public function getProgression()
    {
        switch ($this->getProgress()) {
            case 'download': return 10; break;
            case 'preprocess_xml': return 15; break;
            case 'process_xml': return 20; break;
            case 'produce_rdf': return 40; break;
            case 'persist_rdf': return 75; break;
            case 'reporting': return 100; break;
            default: return 0;
        }
    }

    /**
     * @return StatusInfos
     */
    public function getStatusInfos(): StatusInfos
    {
        if ($this->statusInfos) {
            return $this->statusInfos;
        }

        $status = $this->getStatus();
        $statusInfos = $this->createStatusInfos($status, self::$statusDefinitions[$status][0], self::$statusDefinitions[$status][1]);
        $this->setStatusInfos($statusInfos);

        return $statusInfos;
    }

    /**
     * @return ModeInfos
     */
    public function getModeInfos(): ModeInfos
    {
        if ($this->modeInfos) {
            return $this->modeInfos;
        }

        $mode = $this->getMode();
        $modeInfos = $this->createModeInfos($mode, Flux::$modeDefinitions[$mode][0], Flux::$modeDefinitions[$mode][1]);
        $this->setModeInfos($modeInfos);

        return $modeInfos;
    }

    /**
     * @return int
     */
    public function getLastJob()
    {
        return $this->lastJob;
    }

    /**
     * @param int $lastJob
     *
     * @return Process
     */
    public function setLastJob($lastJob)
    {
        $this->lastJob = $lastJob;

        return $this;
    }

    /**
     * @return Log
     */
    public function getReasonError()
    {
        return $this->reasonError;
    }

    /**
     * @param Log $reasonError
     */
    public function setReasonError($reasonError)
    {
        $this->reasonError = $reasonError;
    }

    public function getPrevSibling()
    {
        /** @var $prevProcess Process */
        $prevProcess = null;
        foreach ($this->getFlux()->getProcesses() as $processes) {
            /** @var $processes Process */
            if ($processes->getUpdatedAt() < $this->getUpdatedAt()) {
                if ($prevProcess === null || ($processes->getUpdatedAt() > $prevProcess->getUpdatedAt())) {
                    $prevProcess = $processes;
                }
            }
        }
        if ($prevProcess !== null && $prevProcess->getId() == $this->getId()) {
            return null;
        }

        return $prevProcess;
    }

    /**
     * @return int
     */
    public function getStatInsert()
    {
        return $this->statInsert;
    }

    /**
     * @param int $statInsert
     */
    public function setStatInsert($statInsert)
    {
        $this->statInsert = $statInsert;
    }

    /**
     * @return int
     */
    public function getStatUpdate()
    {
        return $this->statUpdate;
    }

    /**
     * @param int $statUpdate
     */
    public function setStatUpdate($statUpdate)
    {
        $this->statUpdate = $statUpdate;
    }

    /**
     * @return int
     */
    public function getStatDelete()
    {
        return $this->statDelete;
    }

    /**
     * @param int $statDelete
     */
    public function setStatDelete($statDelete)
    {
        $this->statDelete = $statDelete;
    }

    /**
     * @return int
     */
    public function getStatValid()
    {
        return $this->statValid;
    }

    /**
     * @param int $statValid
     */
    public function setStatValid($statValid)
    {
        $this->statValid = $statValid;
    }

    /**
     * @return int
     */
    public function getStatIgnored()
    {
        return $this->statIgnored;
    }

    /**
     * @param int $statIgnored
     */
    public function setStatIgnored($statIgnored)
    {
        $this->statIgnored = $statIgnored;
    }

    /**
     * @return int
     */
    public function getStatInvalid()
    {
        return $this->statInvalid;
    }

    /**
     * @param int $statInvalid
     */
    public function setStatInvalid($statInvalid)
    {
        $this->statInvalid = $statInvalid;
    }

    /**
     * @return int
     */
    public function getStatNbAnomaliesXML()
    {
        return $this->statNbAnomaliesXML;
    }

    /**
     * @param int $statNbAnomaliesXML
     */
    public function setStatNbAnomaliesXML($statNbAnomaliesXML)
    {
        $this->statNbAnomaliesXML = $statNbAnomaliesXML;
    }

    /**
     * @return string
     */
    public function getStatsProcessXML()
    {
        return $this->statsProcessXML;
    }

    /**
     * @param string $statsProcessXML
     */
    public function setStatsProcessXML($statsProcessXML)
    {
        $this->statsProcessXML = $statsProcessXML;
    }

    /**
     * @return string
     */
    public function getStatsProduceRDF()
    {
        return $this->statsProduceRDF;
    }

    /**
     * @param string $statsProduceRDF
     */
    public function setStatsProduceRDF($statsProduceRDF)
    {
        $this->statsProduceRDF = $statsProduceRDF;
    }

    /**
     * @return string
     */
    public function getStatsPersistRDF()
    {
        return $this->statsPersistRDF;
    }

    /**
     * @param string $statsPersistRDF
     */
    public function setStatsPersistRDF($statsPersistRDF)
    {
        $this->statsPersistRDF = $statsPersistRDF;
    }

    /**
     * @return int
     */
    public function getNbExcludedRdfResources()
    {
        return $this->nbExcludedRdfResources;
    }

    /**
     * @param int $nbExcludedRdfResources
     */
    public function setNbExcludedRdfResources($nbExcludedRdfResources)
    {
        $this->nbExcludedRdfResources = $nbExcludedRdfResources;
    }
}
