#!/bin/sh
#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier: GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

BIN_PATH="`dirname \"$0\"`"
BIN_PATH="`( cd \"$BIN_PATH\" && pwd)`"
if [ -z "$BIN_PATH" ] ; then
  exit 1
fi

cd $BIN_PATH/..
php bin/console fixtures:load
vendor/bin/behat --config behat.yml -p chrome